package main

//leetcode submit region begin(Prohibit modification and deletion)
func longestOnes(nums []int, k int) int {
	ans := 0
	left, cnt0 := 0, 0

	for right, num := range nums {
		cnt0 += 1 - num
		for cnt0 > k {
			cnt0 -= 1 - nums[left]
			left++
		}
		ans = max(ans, right-left+1)
	}
	return ans
}
func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

//leetcode submit region end(Prohibit modification and deletion)
