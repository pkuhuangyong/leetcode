package main

import "encoding/binary"

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func bstFromPreorder(preorder []int) *TreeNode {
	return build(preorder, 0, len(preorder)-1)
}
func build(preorder []int, start int, end int) *TreeNode {
	if start > end {
		return nil
	}
	if start == end {
		return &TreeNode{
			Val:   preorder[start],
			Left:  nil,
			Right: nil,
		}
	}

	rootVal := preorder[start]
	rightRootIdx := -1
	for i := start + 1; i <= end; i++ {
		if preorder[i] > rootVal {
			rightRootIdx = i
			break
		}
	}

	root := &TreeNode{Val: rootVal}
	if rightRootIdx == -1 {
		root.Left = build(preorder, start+1, end)
		root.Right = nil
	} else {
		root.Left = build(preorder, start+1, rightRootIdx-1)
		root.Right = build(preorder, rightRootIdx, end)
	}

	return root
}

//leetcode submit region end(Prohibit modification and deletion)
