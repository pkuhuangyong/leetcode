package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func levelOrder(root *TreeNode) [][]int {
	if root == nil {
		return [][]int{}
	}

	q := NodeQueue{nodes: make([]*withLevel, 0)}
	rootWithLevel := &withLevel{
		node:  root,
		level: 0,
	}
	q.Enqueue(rootWithLevel)
	ans := make([][]int, 0)
	for !q.IsEmpty() {
		l := len(q.nodes)
		levelNodes := make([]int, 0)
		for i := 0; i < l; i++ {
			nodeWithLevel := q.Dequeue()
			levelNodes = append(levelNodes, nodeWithLevel.node.Val)
			if nodeWithLevel.node.Left != nil {
				left := &withLevel{
					node:  nodeWithLevel.node.Left,
					level: nodeWithLevel.level + 1,
				}
				q.Enqueue(left)
			}
			if nodeWithLevel.node.Right != nil {
				right := &withLevel{
					node:  nodeWithLevel.node.Right,
					level: nodeWithLevel.level + 1,
				}
				q.Enqueue(right)
			}
		}
		ans = append(ans, levelNodes)
	}
	return ans
}

func levelOrder1(root *TreeNode) [][]int {
	if root == nil {
		return [][]int{}
	}

	m := make(map[int][]int)

	q := NodeQueue{nodes: make([]*withLevel, 0)}
	rootWithLevel := &withLevel{
		node:  root,
		level: 0,
	}
	q.Enqueue(rootWithLevel)

	for !q.IsEmpty() {
		nodeWithLevel := q.Dequeue()
		if _, ok := m[nodeWithLevel.level]; ok {
			m[nodeWithLevel.level] = append(m[nodeWithLevel.level], nodeWithLevel.node.Val)
		} else {
			m[nodeWithLevel.level] = []int{nodeWithLevel.node.Val}
		}
		if nodeWithLevel.node.Left != nil {
			left := &withLevel{
				node:  nodeWithLevel.node.Left,
				level: nodeWithLevel.level + 1,
			}
			q.Enqueue(left)
		}
		if nodeWithLevel.node.Right != nil {
			right := &withLevel{
				node:  nodeWithLevel.node.Right,
				level: nodeWithLevel.level + 1,
			}
			q.Enqueue(right)
		}
	}
	maxLevel := -1
	for k, _ := range m {
		if k > maxLevel {
			maxLevel = k
		}
	}
	res := make([][]int, 0)
	for i := 0; i <= maxLevel; i++ {
		res = append(res, m[i])
	}
	return res
}

type withLevel struct {
	node  *TreeNode
	level int
}
type NodeQueue struct {
	nodes []*withLevel
}

func (q *NodeQueue) Enqueue(node *withLevel) {
	q.nodes = append(q.nodes, node)
}

func (q *NodeQueue) Dequeue() *withLevel {
	if q.IsEmpty() {
		return nil
	}
	node := q.nodes[0]
	q.nodes = q.nodes[1:]
	return node
}

func (q *NodeQueue) IsEmpty() bool {
	return len(q.nodes) == 0
}

//leetcode submit region end(Prohibit modification and deletion)
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	node15 := &TreeNode{Val: 15}
	node7 := &TreeNode{Val: 7}
	node20 := &TreeNode{
		Val:   20,
		Left:  node15,
		Right: node7,
	}
	node9 := &TreeNode{Val: 9}
	node3 := &TreeNode{
		Val:   3,
		Left:  node9,
		Right: node20,
	}

	fmt.Println(levelOrder(node3))
}
