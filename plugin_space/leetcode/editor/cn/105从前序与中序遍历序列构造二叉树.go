package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
var valToIndex = make(map[int]int)

func buildTree(preorder []int, inorder []int) *TreeNode {
	for i, val := range inorder {
		valToIndex[val] = i
	}
	return build(preorder, 0, len(preorder)-1, inorder, 0, len(inorder)-1)
}

func build(preorder []int, preStart int, preEnd int,
	inorder []int, inStart int, inEnd int) *TreeNode {
	if preStart > preEnd {
		return nil
	}

	rootVal := preorder[preStart]
	root := &TreeNode{Val: rootVal}
	rootValIdx := valToIndex[rootVal]
	leftSize := rootValIdx - inStart
	root.Left = build(preorder, preStart+1, preStart+leftSize, inorder, inStart, rootValIdx-1)
	root.Right = build(preorder, preStart+leftSize+1, preEnd, inorder, rootValIdx+1, inEnd)
	return root
}

//leetcode submit region end(Prohibit modification and deletion)
