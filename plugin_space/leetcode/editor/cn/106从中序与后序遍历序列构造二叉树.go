package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
var valToIndex = make(map[int]int)

func buildTree(inorder []int, postorder []int) *TreeNode {
	for i, val := range inorder {
		valToIndex[val] = i
	}
	return build(inorder, 0, len(inorder)-1, postorder, 0, len(postorder)-1)
}

func build(inorder []int, inStart int, inEnd int,
	postorder []int, postStart int, postEnd int) *TreeNode {
	if inStart > inEnd {
		return nil
	}
	rootVal := postorder[postEnd]
	rootValIdx := valToIndex[rootVal]
	rightSize := inEnd - rootValIdx

	root := &TreeNode{Val: rootVal}
	root.Left = build(inorder, inStart, rootValIdx-1, postorder, postStart, postEnd-rightSize-1)
	root.Right = build(inorder, rootValIdx+1, inEnd, postorder, postEnd-rightSize, postEnd-1)

	return root
}

//leetcode submit region end(Prohibit modification and deletion)
