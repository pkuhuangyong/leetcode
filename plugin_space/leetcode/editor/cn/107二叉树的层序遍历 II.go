package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func levelOrderBottom(root *TreeNode) [][]int {
	if root == nil {
		return [][]int{}
	}

	q := NodeQueue{nodes: make([]*withLevel, 0)}
	rootWithLevel := &withLevel{
		node:  root,
		level: 0,
	}
	q.Enqueue(rootWithLevel)
	ans := make([][]int, 0)
	for !q.IsEmpty() {
		l := len(q.nodes)
		levelNodes := make([]int, 0)
		for i := 0; i < l; i++ {
			nodeWithLevel := q.Dequeue()
			levelNodes = append(levelNodes, nodeWithLevel.node.Val)
			if nodeWithLevel.node.Left != nil {
				left := &withLevel{
					node:  nodeWithLevel.node.Left,
					level: nodeWithLevel.level + 1,
				}
				q.Enqueue(left)
			}
			if nodeWithLevel.node.Right != nil {
				right := &withLevel{
					node:  nodeWithLevel.node.Right,
					level: nodeWithLevel.level + 1,
				}
				q.Enqueue(right)
			}
		}
		ans = append([][]int{levelNodes}, ans...)
	}
	return ans
}

type withLevel struct {
	node  *TreeNode
	level int
}
type NodeQueue struct {
	nodes []*withLevel
}

func (q *NodeQueue) Enqueue(node *withLevel) {
	q.nodes = append(q.nodes, node)
}

func (q *NodeQueue) Dequeue() *withLevel {
	if q.IsEmpty() {
		return nil
	}
	node := q.nodes[0]
	q.nodes = q.nodes[1:]
	return node
}

func (q *NodeQueue) IsEmpty() bool {
	return len(q.nodes) == 0
}

//leetcode submit region end(Prohibit modification and deletion)
