package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func sortedArrayToBST(nums []int) *TreeNode {
	return build(nums, 0, len(nums)-1)
}

func build(nums []int, start int, end int) *TreeNode {
	if start > end {
		return nil
	}

	rootIdx := start + (end-start)/2
	root := &TreeNode{Val: nums[rootIdx]}
	root.Left = build(nums, start, rootIdx-1)
	root.Right = build(nums, rootIdx+1, end)
	return root
}

//leetcode submit region end(Prohibit modification and deletion)
