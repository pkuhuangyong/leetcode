package main

import "math"

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func minDepth(root *TreeNode) int {
	return helpMin(root)
}

func helpMin(root *TreeNode) int {
	if root == nil {
		return 0
	}
	if root.Left == nil && root.Right == nil {
		return 1
	}

	minD := math.MaxInt64
	if root.Left != nil {
		leftMin := helpMin(root.Left)
		minD = getMin(minD, leftMin)
	}
	if root.Right != nil {
		rightMin := helpMin(root.Right)
		minD = getMin(minD, rightMin)
	}
	return minD + 1
}
func getMin(x, y int) int {
	if x < y {
		return x
	}
	return y
}

//leetcode submit region end(Prohibit modification and deletion)
