package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func hasPathSum(root *TreeNode, targetSum int) bool {

	if root == nil {
		return false
	}

	if root.Left == nil && root.Right == nil {
		return root.Val == targetSum
	}

	if root.Left != nil {
		left := hasPathSum(root.Left, targetSum-root.Val)
		if left {
			return true
		}
	}

	if root.Right != nil {
		right := hasPathSum(root.Right, targetSum-root.Val)
		if right {
			return true
		}
	}

	return false
}

//leetcode submit region end(Prohibit modification and deletion)
