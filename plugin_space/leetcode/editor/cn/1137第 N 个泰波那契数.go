package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func tribonacci(n int) int {
	t := []int{0, 1, 1}

	for i := 3; i <= n; i++ {
		t[i%3] = t[0] + t[1] + t[2]
	}
	return t[n%3]
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	fmt.Println(tribonacci(25))
}
