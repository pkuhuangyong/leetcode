package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func pathSum(root *TreeNode, targetSum int) [][]int {
	ans := make([][]int, 0)

	var backtrack func(path []int, entry *TreeNode, target int)
	backtrack = func(path []int, entry *TreeNode, target int) {
		if entry == nil {
			return
		}
		if entry.Left == nil && entry.Right == nil {
			if entry.Val == target {
				temp := make([]int, len(path))
				copy(temp, path)
				temp = append(temp, entry.Val)
				ans = append(ans, temp)
			}
			return
		}

		path = append(path, entry.Val)

		if entry.Left != nil {
			backtrack(path, entry.Left, target-entry.Val)
			//path = path[:len(path)-1]
		}
		if entry.Right != nil {
			backtrack(path, entry.Right, target-entry.Val)
			//path = path[:len(path)-1]
		}

	}
	path := make([]int, 0)
	backtrack(path, root, targetSum)
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	node2 := &TreeNode{Val: 2}
	node13 := &TreeNode{Val: 13}
	node1 := &TreeNode{Val: 1}
	node5 := &TreeNode{Val: 5}
	node7 := &TreeNode{Val: 7}
	node4 := &TreeNode{
		Val:   4,
		Left:  node5,
		Right: node1,
	}

	node11 := &TreeNode{
		Val:   11,
		Left:  node7,
		Right: node2,
	}
	node8 := &TreeNode{
		Val:   8,
		Left:  node13,
		Right: node4,
	}
	node40 := &TreeNode{
		Val:   4,
		Left:  node11,
		Right: nil,
	}
	node50 := &TreeNode{
		Val:   5,
		Left:  node40,
		Right: node8,
	}
	targetSum := 22

	fmt.Println(pathSum(node50, targetSum))
}
