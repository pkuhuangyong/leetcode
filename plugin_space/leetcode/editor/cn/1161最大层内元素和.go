package main

import (
	"fmt"
	"math"
)

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func maxLevelSum(root *TreeNode) int {
	sums := make([]int, 1)
	sums[0] = math.MinInt32

	queue := make([]*TreeNode, 0)
	queue = append(queue, root)

	for len(queue) > 0 {
		l := len(queue)

		levelSum := 0
		for i := 0; i < l; i++ {
			node := queue[0]
			queue = queue[1:]
			levelSum += node.Val
			if node.Left != nil {
				queue = append(queue, node.Left)
			}
			if node.Right != nil {
				queue = append(queue, node.Right)
			}
		}
		sums = append(sums, levelSum)
	}

	maxLevel := 0
	maxSum := sums[0]
	for i := 0; i < len(sums); i++ {
		if maxSum < sums[i] {
			maxSum = sums[i]
			maxLevel = i
		}
	}
	return maxLevel
}

//leetcode submit region end(Prohibit modification and deletion)
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	node_8 := &TreeNode{Val: -8}
	node71 := &TreeNode{Val: 7}
	node72 := &TreeNode{
		Val:   7,
		Left:  node71,
		Right: node_8,
	}
	node0 := &TreeNode{Val: 0}
	node1 := &TreeNode{
		Val:   1,
		Left:  node72,
		Right: node0,
	}

	fmt.Println(maxLevelSum(node1))
}
