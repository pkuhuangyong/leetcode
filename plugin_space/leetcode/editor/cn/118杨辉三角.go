package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func generate(numRows int) [][]int {
	yhTriangle := make([][]int, 0)
	yhTriangle = append(yhTriangle, []int{1})

	if numRows == 1 {
		return yhTriangle
	} else if numRows == 2 {
		yhTriangle = append(yhTriangle, []int{1, 1})
		return yhTriangle
	}
	yhTriangle = append(yhTriangle, []int{1, 1})

	for i := 2; i < numRows; i++ {
		row := make([]int, 0)
		row = append(row, 1)

		for j := 1; j < i; j++ {
			row = append(row, yhTriangle[i-1][j-1]+yhTriangle[i-1][j])
		}

		row = append(row, 1)
		yhTriangle = append(yhTriangle, row)
	}
	return yhTriangle
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	fmt.Println(generate(5))
}
