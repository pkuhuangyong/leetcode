package main

//leetcode submit region begin(Prohibit modification and deletion)
func maxArea(height []int) int {
	maxA := 0
	i, j := 0, len(height)-1
	for i <= j {
		if height[i] <= height[j] {
			area := height[i] * (j - i)
			maxA = max(maxA, area)
			i++
		} else {
			area := height[j] * (j - i)
			maxA = max(maxA, area)
			j--
		}
	}
	return maxA
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

//leetcode submit region end(Prohibit modification and deletion)
