package main

//leetcode submit region begin(Prohibit modification and deletion)
func uniqueOccurrences(arr []int) bool {
	counts := make(map[int]int)
	for _, num := range arr {
		if _, ok := counts[num]; ok {
			counts[num] = counts[num] + 1
		} else {
			counts[num] = 1
		}
	}

	dup := make(map[int]struct{})
	for _, v := range counts {
		dup[v] = struct{}{}
	}
	return len(dup) == len(counts)
}

//leetcode submit region end(Prohibit modification and deletion)
