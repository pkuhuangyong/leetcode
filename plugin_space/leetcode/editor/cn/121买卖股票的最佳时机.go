package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func maxProfit(prices []int) int {
	if len(prices) == 1 {
		return 0
	}
	maxPro, minPri := 0, prices[0]
	for i := 0; i < len(prices); i++ {
		if prices[i] < minPri {
			minPri = prices[i]
		}
		pro := prices[i] - minPri
		if maxPro < pro {
			maxPro = pro
		}
	}
	return maxPro
}

func getFirstPeak(prices []int, start int) int {
	if start > len(prices)-1 {
		return len(prices) - 1
	}
	for i := start; i < len(prices); i++ {
		if i == 0 {
			continue
		}
		if prices[i] < prices[i-1] {
			return i - 1
		}
	}
	return len(prices) - 1
}

func getFirstVally(prices []int, start int) int {

	if start > len(prices)-1 {
		return len(prices) - 1
	}
	for i := start; i < len(prices); i++ {
		if i == 0 {
			continue
		}
		if prices[i] > prices[i-1] {
			return i - 1
		}
	}
	return len(prices) - 1
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	prices := []int{7, 1, 5, 3, 6, 4}

	fmt.Println(maxProfit(prices))
}
