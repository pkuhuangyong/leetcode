package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func maxProfit(prices []int) int {
	cur := 0
	profit := 0
	for true {
		if len(prices[cur:]) == 0 {
			break
		}

		buyP, buySkip := getBuyPoint(prices[cur:])
		cur = cur + buySkip

		if len(prices[cur:]) == 0 {
			break
		}
		sellP, sellSkip := getSellPoint(prices[cur:])
		cur = cur + sellSkip

		profit = profit + (sellP - buyP)

		fmt.Printf("got profit: %d until %dth day.\n", profit, cur)

	}
	return profit
}

func getSellPoint(prices []int) (sellP int, skip int) {
	m := 0
	for i, price := range prices {
		if prices[m] <= price {
			m = i
		} else {
			break
		}
	}
	sellP = prices[m]
	skip = m + 1
	fmt.Printf("found sell point at price: %d\n", sellP)
	return sellP, skip
}
func getBuyPoint(prices []int) (buyP int, skip int) {
	l := 0
	for i, price := range prices {
		if prices[l] >= price {
			l = i
		} else {
			break
		}
	}

	buyP = prices[l]
	skip = l + 1
	fmt.Printf("found buy point at price: %d\n", buyP)
	return buyP, skip
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	prices := []int{1, 2, 3, 4, 5}

	fmt.Println(maxProfit(prices))
}
