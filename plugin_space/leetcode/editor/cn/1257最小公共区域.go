package main

//leetcode submit region begin(Prohibit modification and deletion)
func findSmallestRegion(regions [][]string, region1 string, region2 string) string {

	tree := getTree(regions)

	p1, p2 := region1, region2
	for p1 != p2 {
		if _, ok := tree[p1]; !ok {
			p1 = region2
		} else {
			p1 = tree[p1]
		}

		if _, ok := tree[p2]; !ok {
			p2 = region1
		} else {
			p2 = tree[p2]
		}
	}

	return p1
}

func getTree(regions [][]string) map[string]string {
	var tree = make(map[string]string, 0)
	for _, region := range regions {
		for i := 1; i < len(region); i++ {
			tree[region[i]] = region[0]
		}
	}
	return tree
}

func slicePath(tree map[string]string, region string, path *[]string) {
	*path = append(*path, region)
	if _, ok := tree[region]; !ok {
		return
	}

	slicePath(tree, tree[region], path)
}

func mapPath(tree map[string]string, region string, path *map[string]int) {
	(*path)[region] = 1
	if _, ok := tree[region]; !ok {
		return
	}

	mapPath(tree, tree[region], path)
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	regions := [][]string{
		{"Earth", "North America", "South America"},
		{"North America", "United States", "Canada"},
		{"United States", "New York", "Boston"},
		{"Canada", "Ontario", "Quebec"},
		{"South America", "Brazil"},
	}
	region1 := "United States"
	region2 := "New York"

	findSmallestRegion(regions, region1, region2)
}
