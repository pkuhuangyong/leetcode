package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func isPalindrome(s string) bool {
	var left, right uint8
	for i, j := 0, len(s)-1; i < j; {
		for !isLegal(s[i]) && i < j {
			i++
		}
		if 65 <= s[i] && s[i] <= 90 {
			left = s[i] + 32
		} else {
			left = s[i]
		}

		for !isLegal(s[j]) && i < j {
			j--
		}
		if 65 <= s[j] && s[j] <= 90 {
			right = s[j] + 32
		} else {
			right = s[j]
		}
		fmt.Println(left, right)
		if left != right {
			return false
		}

		i++
		j--

	}
	return true
}

func isLegal(ch uint8) bool {
	if 48 <= ch && ch <= 57 {
		return true
	}
	if 65 <= ch && ch <= 90 {
		return true
	}
	if 97 <= ch && ch <= 122 {
		return true
	}

	return false
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	s := ".,,"
	isPalindrome(s)
}
