package main

import "sort"

//leetcode submit region begin(Prohibit modification and deletion)
func suggestedProducts(products []string, searchWord string) [][]string {
	ans := make([][]string, 0)
	sort.Strings(products)
	trie := Constructor()
	for _, word := range products {
		trie.Insert(word)
	}

	for i := 1; i <= len(searchWord); i++ {
		prefix := searchWord[:i]
		recommend3 := trie.Recommend(prefix)
		ans = append(ans, recommend3)
	}
	return ans
}

type Trie struct {
	isWord     bool
	children   []*Trie
	recommend3 []string
}

func Constructor() Trie {
	return Trie{
		isWord:     false,
		children:   make([]*Trie, 26),
		recommend3: []string{},
	}
}
func (this *Trie) Recommend(prefix string) []string {
	cur := this
	recommend := make([]string, 0)
	for i := 0; i < len(prefix); i++ {
		idx := prefix[i] - 'a'
		if cur.children[idx] == nil {
			return recommend
		}
		cur = cur.children[idx]
	}
	recommend = cur.recommend3
	return recommend
}
func (this *Trie) Insert(word string) {
	cur := this
	for i := 0; i < len(word); i++ {
		idx := word[i] - 'a'
		if cur.children[idx] == nil {
			cur.children[idx] = &Trie{
				isWord:   false,
				children: make([]*Trie, 26),
			}
		}
		cur = cur.children[idx]
		if len(cur.recommend3) < 3 {
			cur.recommend3 = append(cur.recommend3, word)
		}
	}
	cur.isWord = true
}

func (this *Trie) Search(word string) bool {
	cur := this
	for i := 0; i < len(word); i++ {
		idx := word[i] - 'a'
		if cur.children[idx] == nil {
			return false
		}
		cur = cur.children[idx]
	}
	return cur.isWord
}

func (this *Trie) StartsWith(prefix string) bool {
	cur := this
	for i := 0; i < len(prefix); i++ {
		idx := prefix[i] - 'a'
		if cur.children[idx] == nil {
			return false
		}
		cur = cur.children[idx]
	}
	return true
}

//leetcode submit region end(Prohibit modification and deletion)
