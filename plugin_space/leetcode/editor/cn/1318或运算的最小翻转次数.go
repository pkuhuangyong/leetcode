package main

//leetcode submit region begin(Prohibit modification and deletion)
func minFlips(a int, b int, c int) int {
	ans := 0
	for i := 0; i < 31; i++ {
		bitA := (a >> i) & 1
		bitB := (b >> i) & 1
		bitC := (c >> i) & 1

		if bitC == 0 {
			ans += bitA + bitB
		} else {
			if bitA == 0 && bitB == 0 {
				ans += 1
			}
		}
	}

	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
