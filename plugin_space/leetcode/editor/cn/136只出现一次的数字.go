package main

//leetcode submit region begin(Prohibit modification and deletion)
func singleNumber(nums []int) int {

	xor := 0

	for _, num := range nums {
		xor ^= num
		//xor = xor ^ num
	}

	return xor
}

//leetcode submit region end(Prohibit modification and deletion)
