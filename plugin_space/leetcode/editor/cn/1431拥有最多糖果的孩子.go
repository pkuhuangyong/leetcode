package main

//leetcode submit region begin(Prohibit modification and deletion)
func kidsWithCandies(candies []int, extraCandies int) []bool {
	oldMax := -1
	for i := 0; i < len(candies); i++ {
		if oldMax < candies[i] {
			oldMax = candies[i]
		}
		candies[i] = candies[i] + extraCandies
	}
	ans := make([]bool, len(candies))
	for i := 0; i < len(candies); i++ {
		if candies[i] >= oldMax {
			ans[i] = true
		}
	}

	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
