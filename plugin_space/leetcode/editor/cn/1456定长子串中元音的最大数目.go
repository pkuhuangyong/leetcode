package main

//leetcode submit region begin(Prohibit modification and deletion)
func maxVowels(s string, k int) int {
	m := map[byte]struct{}{
		'a': {},
		'e': {},
		'i': {},
		'o': {},
		'u': {},
	}

	maxV := 0
	for i := 0; i < k; i++ {
		if _, ok := m[s[i]]; ok {
			maxV++
		}
	}
	count := maxV
	for i := k; i < len(s); i++ {
		if _, ok := m[s[i]]; ok {
			count++
		}
		if _, ok := m[s[i-k]]; ok {
			count--
		}
		maxV = max(maxV, count)
	}
	return maxV
}
func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

//leetcode submit region end(Prohibit modification and deletion)
