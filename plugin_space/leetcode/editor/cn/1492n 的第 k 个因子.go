package main

import (
	"math"
)

//leetcode submit region begin(Prohibit modification and deletion)
func kthFactor(n int, k int) int {
	sqr := int(math.Sqrt(float64(n)))
	pre := make([]int, sqr)
	count := 1
	pre[0] = 1
	for i := 2; i <= sqr; i++ {
		if n%i == 0 {
			pre[count] = i
			count++
		}
	}

	pre = pre[:count]
	if k <= len(pre) {
		return pre[k-1]
	}
	all := 2 * count
	if sqr*sqr == n {
		all--
	}

	if k > all {
		return -1
	} else {
		return n / pre[all-k]
	}
}

//leetcode submit region end(Prohibit modification and deletion)
