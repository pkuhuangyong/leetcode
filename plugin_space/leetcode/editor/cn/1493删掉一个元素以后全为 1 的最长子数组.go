package main

//leetcode submit region begin(Prohibit modification and deletion)
func longestSubarray(nums []int) int {
	ans := 0
	left, cnt0 := 0, 0
	for right, num := range nums {
		cnt0 += 1 - num
		for cnt0 > 1 {
			cnt0 -= 1 - nums[left]
			left++
		}
		if right-left > ans {
			ans = right - left
		}
	}
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
