package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func longestCommonPrefix(strs []string) string {
	if len(strs) == 0 {
		return ""
	} else if len(strs) == 1 {
		return strs[0]
	}
	res := ""
	minLength := len(strs[0]) + 1
	for _, str := range strs {
		if len(str) < minLength {
			minLength = len(str)
		}
	}

	for i := 0; i < minLength; i++ {
		for _, str := range strs {
			if str[i] != strs[0][i] {
				return res
			}
		}
		res += string(strs[0][i])
	}
	return res
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	strs := []string{"flower", "flow", "flight", "dog", "racecar", "car"}
	fmt.Println(longestCommonPrefix(strs))
}
