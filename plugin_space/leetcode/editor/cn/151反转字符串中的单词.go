package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func reverseWords(s string) string {

	list := getWordsList(s)
	ans := ""
	for i := len(list) - 1; i > 0; i-- {
		ans += list[i]
		ans += " "
	}
	ans += list[0]
	return ans
}

func getWordsList(s string) []string {
	list := make([]string, 0)

	word := make([]byte, 0)

	var pre uint8 = ' '
	for i := 0; i < len(s); i++ {
		if s[i] != ' ' {
			// 单词字母
			word = append(word, s[i])
		} else {
			if pre != ' ' {
				temp := string(word)
				list = append(list, temp)
				word = word[:0]
			}
		}
		pre = s[i]
	}
	if len(word) > 0 {
		list = append(list, string(word))
	}

	return list
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	//s := "the sky is blue"
	s1 := "  hello world  "
	s2 := "a good   example"
	//fmt.Println(reverseWords(s))
	fmt.Println(reverseWords(s1))
	fmt.Println(reverseWords(s2))
}
