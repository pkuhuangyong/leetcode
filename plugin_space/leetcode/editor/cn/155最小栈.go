package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
type MinStack struct {
	stack []int
	minE  int
}

func Constructor() MinStack {
	return MinStack{
		stack: make([]int, 0),
		minE:  0,
	}
}

func (this *MinStack) Push(val int) {
	if len(this.stack) == 0 {
		this.minE = val
		this.stack = append(this.stack, 0)
		return
	}
	delta := val - this.minE
	this.stack = append(this.stack, delta)
	if delta < 0 {
		this.minE = val
	}
}

func (this *MinStack) Pop() {
	delta := this.stack[len(this.stack)-1]
	if delta < 0 {
		this.minE = this.minE - delta
	}
	this.stack = this.stack[:len(this.stack)-1]
}

func (this *MinStack) Top() int {
	delta := this.stack[len(this.stack)-1]
	if delta < 0 {
		return this.minE
	}
	return delta + this.minE
}

func (this *MinStack) GetMin() int {
	return this.minE
}

/**
 * Your MinStack object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Push(val);
 * obj.Pop();
 * param_3 := obj.Top();
 * param_4 := obj.GetMin();
 */
//leetcode submit region end(Prohibit modification and deletion)
func main() {
	obj := Constructor()
	obj.Push(2)
	obj.Push(0)
	obj.Push(3)
	obj.Push(0)
	fmt.Println(obj.GetMin())
	obj.Pop()
	fmt.Println(obj.GetMin())
	obj.Pop()
	fmt.Println(obj.GetMin())
	obj.Pop()
	fmt.Println(obj.GetMin())
}
