package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

//leetcode submit region begin(Prohibit modification and deletion)

func upsideDownBinaryTree(root *TreeNode) *TreeNode {
	if root == nil || root.Left == nil {
		return root
	}

	var stk = make([]*TreeNode, 10)
	cur := root
	i := 0
	for cur.Left != nil {
		stk[i] = cur
		cur = cur.Left
		i++
	}
	newRoot := cur
	for ; i > 0; i-- {
		parent := stk[i-1]

		cur.Left = parent.Right
		cur.Right = parent
		cur = parent
	}
	cur.Left = nil
	cur.Right = nil

	return newRoot
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	node4 := TreeNode{
		Val:   4,
		Left:  nil,
		Right: nil,
	}
	node5 := TreeNode{
		Val:   5,
		Left:  nil,
		Right: nil,
	}
	node2 := TreeNode{
		Val:   2,
		Left:  &node4,
		Right: &node5,
	}
	node3 := TreeNode{
		Val:   3,
		Left:  nil,
		Right: nil,
	}
	node1 := TreeNode{
		Val:   1,
		Left:  &node2,
		Right: &node3,
	}

	tree := upsideDownBinaryTree(&node1)
	fmt.Println(tree, node1)
}
