package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * The read4 API is already defined for you.
 *
 *     read4 := func(buf4 []byte) int
 *
 * // Below is an example of how the read4 API can be called.
 * file := File("abcdefghijk") // File is "abcdefghijk", initially file pointer (fp) points to 'a'
 * buf4 := make([]byte, 4) // Create buffer with enough space to store characters
 * read4(buf4) // read4 returns 4. Now buf = ['a','b','c','d'], fp points to 'e'
 * read4(buf4) // read4 returns 4. Now buf = ['e','f','g','h'], fp points to 'i'
 * read4(buf4) // read4 returns 3. Now buf = ['i','j','k',...], fp points to end of file
 */

var solution = func(read4 func([]byte) int) func([]byte, int) int {

	buffer := make([]byte, 0)

	// implement read below.
	return func(buf []byte, n int) int {
		fmt.Println("buffer when call:", buffer)

		if n <= len(buffer) {
			buf = buffer[:n]
			fmt.Println("buf all use buffer:\t", buf)
			buffer = buffer[n:]
			fmt.Println("buffer before return:", buffer)
			return n
		}
		fmt.Println("default buf:\t", buf)

		buf4 := make([]byte, 4)
		for i, v := range buffer {
			buf[i] = v
		}
		total := len(buffer)

		for n > total {
			m := read4(buf4)
			fmt.Println("buf4:\t", buf4)

			if m < 4 {
				for i := 0; i < m; i++ {
					buf[total+i] = buf4[i]
				}
				total += m
				fmt.Println("total=", total, ",\tbuf:", buf)
				if total > n {
					buffer = buf[n:total]
					buf = buf[:n]
					fmt.Println("buffer before return:", buffer)
					return n
				} else {
					buffer = buffer[:0]
					return total
				}
			} else {
				for i, v := range buf4 {
					buf[total+i] = v
				}
				total += 4
			}
		}
		buffer = buf[n:total]
		buf = buf[:n]
		fmt.Println("buffer before return:", buffer)
		return n
	}
}

//leetcode submit region end(Prohibit modification and deletion)
