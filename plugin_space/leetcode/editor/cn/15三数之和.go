package main

import (
	"fmt"
	"sort"
	"strconv"
)

//leetcode submit region begin(Prohibit modification and deletion)
func threeSum(nums []int) [][]int {
	res := make([][]int, 0)
	tb := make(map[string][]int)
	for i, n := range nums {
		target := 0 - n
		twoIndices := twoSumHelper(nums, target)
		//fmt.Println(twoIndices)
		if twoIndices == nil {
			continue
		}

		for _, twoIndex := range twoIndices {
			if i != twoIndex[0] && i != twoIndex[1] {
				candidate := []int{n, nums[twoIndex[0]], nums[twoIndex[1]]}
				tb[genKey(candidate)] = candidate
			}
		}

	}
	for _, vr := range tb {
		res = append(res, vr)
	}
	return res
}

func twoSumHelper(nums []int, target int) [][]int {
	tb := make(map[int]int)
	res := make([][]int, 0)
	resSet := make(map[string][]int)
	for i, n := range nums {
		diff := target - n
		if id, ok := tb[diff]; ok {
			candidate := []int{n, diff}
			resSet[genKey(candidate)] = []int{id, i}
			delete(tb, diff)
		} else {
			tb[n] = i
		}
	}

	for _, vr := range resSet {
		res = append(res, vr)
	}

	return res
}
func genKey(candidate []int) string {
	sort.Ints(candidate)
	s := ""
	for _, n := range candidate {
		s += strconv.Itoa(n)
	}
	return s
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	nums := []int{-1, 0, 1, 2, -1, -4, -2, -3, 3, 0, 4}
	fmt.Println(threeSum(nums))
}
