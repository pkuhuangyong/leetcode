package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func isOneEditDistance(s string, t string) bool {
	ls, lt := len(s), len(t)

	if s == "" {
		return lt == 1
	}
	if t == "" {
		return ls == 1
	}

	if (ls-lt > 1) || (lt-ls > 1) {
		return false
	}

	check := func(i int, j int) bool {
		if ls > lt {
			return s[i+1:] == t[j:]
		} else if ls == lt {
			return s[i+1:] == t[j+1:]
		} else {
			return s[i:] == t[j+1:]
		}
	}

	i, j := 0, 0
	for i < ls && j < lt {
		if s[i] != t[j] {
			return check(i, j)
		}
		i++
		j++
	}
	if ls == lt {
		return false
	} else {
		return check(i, j)
	}
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	s := "cab"
	t := "ab"
	fmt.Println(isOneEditDistance(s, t))
}
