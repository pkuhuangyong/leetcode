package main

//leetcode submit region begin(Prohibit modification and deletion)
func findMissingRanges(nums []int, lower int, upper int) [][]int {
	if len(nums) == 0 {
		return [][]int{
			{lower, upper},
		}
	}

	res := make([][]int, 0)

	if nums[0]-lower > 0 {
		res = append(res, []int{lower, nums[0] - 1})
	}

	i, j := 0, 1

	for j < len(nums) {
		if nums[j]-nums[i] > 1 {
			res = append(res, []int{nums[i] + 1, nums[j] - 1})
		}
		i++
		j++
	}

	if upper-nums[j-1] > 0 {
		res = append(res, []int{nums[j-1] + 1, upper})
	}

	return res
}

//leetcode submit region end(Prohibit modification and deletion)
