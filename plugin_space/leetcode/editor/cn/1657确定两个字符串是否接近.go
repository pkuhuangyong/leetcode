package main

import "sort"

//leetcode submit region begin(Prohibit modification and deletion)
func closeStrings(word1 string, word2 string) bool {
	if len(word1) != len(word2) {
		return false
	}
	m1 := make(map[byte]int)
	m2 := make(map[byte]int)

	for i := range word1 {
		m1[word1[i]] += 1
	}
	for i := range word2 {
		m2[word2[i]] += 1
	}
	v1s := make([]int, 0)
	v2s := make([]int, 0)

	for k, v := range m1 {
		if m2[k] < 1 {
			return false
		}
		v1s = append(v1s, v)
	}
	for k, v := range m2 {
		if m1[k] < 1 {
			return false
		}
		v2s = append(v2s, v)
	}
	sort.Ints(v1s)
	sort.Ints(v2s)
	if len(v1s) != len(v2s) {
		return false
	}
	for i := 0; i < len(v1s); i++ {
		if v1s[i] != v2s[i] {
			return false
		}
	}
	return true
}

//leetcode submit region end(Prohibit modification and deletion)
