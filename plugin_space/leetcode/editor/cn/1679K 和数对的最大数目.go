package main

//leetcode submit region begin(Prohibit modification and deletion)
func maxOperations(nums []int, k int) int {
	m := make(map[int]int)
	count := 0

	for _, num := range nums {
		if m[k-num] > 0 {
			m[k-num]--
			count++
		} else {
			m[num]++
		}
	}
	return count
}

//leetcode submit region end(Prohibit modification and deletion)
