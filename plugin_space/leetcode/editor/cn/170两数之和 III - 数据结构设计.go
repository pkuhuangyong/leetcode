package main

//leetcode submit region begin(Prohibit modification and deletion)

type TwoSum struct {
	set map[int]int
}

func Constructor() TwoSum {
	return TwoSum{map[int]int{}}
}

func (this *TwoSum) Add(number int) {
	if _, ok := this.set[number]; ok {
		this.set[number] = this.set[number] + 1
	} else {
		this.set[number] = 1
	}
}

func (this *TwoSum) Find(value int) bool {
	for k := range this.set {
		if _, ok := this.set[value-k]; ok {
			if k == value-k {
				if this.set[k] > 1 {
					return true
				}
			} else {
				return true
			}
		}
	}
	return false
}

/**
 * Your TwoSum object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Add(number);
 * param_2 := obj.Find(value);
 */
//leetcode submit region end(Prohibit modification and deletion)
