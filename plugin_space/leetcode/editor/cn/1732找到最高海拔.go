package main

//leetcode submit region begin(Prohibit modification and deletion)
func largestAltitude(gain []int) int {
	maxAlti := 0
	altitudes := make([]int, len(gain)+1)
	for i := 1; i < len(altitudes); i++ {
		altitudes[i] = altitudes[i-1] + gain[i-1]
		if maxAlti < altitudes[i] {
			maxAlti = altitudes[i]
		}
	}
	return maxAlti
}

//leetcode submit region end(Prohibit modification and deletion)
