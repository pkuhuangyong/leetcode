package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func mergeAlternately(word1 string, word2 string) string {
	ans := ""

	crossTimes := len(word2)
	if len(word1) <= len(word2) {
		crossTimes = len(word1)
	}

	for i := 0; i < crossTimes; i++ {
		ans += string(word1[i])
		ans += string(word2[i])
	}

	ans += word1[crossTimes:]
	ans += word2[crossTimes:]
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	word1 := "abcdefgh"
	word2 := "pqrs"
	fmt.Println(mergeAlternately(word1, word2))
}
