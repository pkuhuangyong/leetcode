package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func letterCombinations(digits string) []string {
	if len(digits) == 0 {
		return []string{}
	}

	length := len(digits)
	ans := make([]string, 0)
	m := make(map[byte][]byte)
	m['2'] = []byte("abc")
	m['3'] = []byte("def")
	m['4'] = []byte("ghi")
	m['5'] = []byte("jkl")
	m['6'] = []byte("mno")
	m['7'] = []byte("pqrs")
	m['8'] = []byte("tuv")
	m['9'] = []byte("wxyz")
	digitBytes := []byte(digits)

	var backtrack func(comb []byte, idx int, used []bool)
	backtrack = func(comb []byte, idx int, used []bool) {
		if len(comb) == length {
			temp := string(comb)
			ans = append(ans, temp)
			return
		}
		k := digitBytes[idx]

		for i := 0; i < len(m[k]); i++ {
			if used[i] {
				continue
			}

			comb = append(comb, m[k][i])
			used[i] = true
			var usedNext []bool
			if idx+1 < len(digitBytes) {
				usedNext = make([]bool, len(m[digitBytes[idx+1]]))
			}
			backtrack(comb, idx+1, usedNext)
			used[i] = false
			comb = comb[:len(comb)-1]
		}

	}
	used := make([]bool, len(m[digitBytes[0]]))
	comb := make([]byte, 0)
	backtrack(comb, 0, used)
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	digits := "23"
	fmt.Println(letterCombinations(digits))
}
