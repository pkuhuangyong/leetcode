package main

//leetcode submit region begin(Prohibit modification and deletion)
func reverseWords(s []byte) {
	if len(s) < 2 {
		return
	}

	findNextEnd := func(cur int) int {
		for cur < len(s) && s[cur] != ' ' {
			cur++
		}
		return cur - 1
	}

	reverse(s, 0, len(s)-1)
	start := 0
	end := findNextEnd(start)

	for end < len(s) {
		reverse(s, start, end)
		start = end + 2
		end = findNextEnd(start)
	}

}

func reverse(s []byte, start int, end int) {
	for start < end {
		temp := s[start]
		s[start] = s[end]
		s[end] = temp
		start++
		end--
	}
}

//leetcode submit region end(Prohibit modification and deletion)
