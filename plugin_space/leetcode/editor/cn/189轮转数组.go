package main

//leetcode submit region begin(Prohibit modification and deletion)
func rotate(nums []int, k int) {
	l := len(nums)
	r := k % l
	newNums := append(nums[l-r:], nums[:l-r]...)
	copy(nums, newNums)
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	nums := []int{
		1, 2, 3, 4, 5, 6, 7,
	}
	rotate(nums, 3)
}
