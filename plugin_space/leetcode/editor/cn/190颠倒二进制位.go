package main

//leetcode submit region begin(Prohibit modification and deletion)
func reverseBits(num uint32) uint32 {
	var rev uint32 = 0
	for i := 0; i < 32 && num > 0; i++ {
		// 低位置 0，保留最后一位
		temp := num & 1
		// temp 左移 31-i 位，注意到循环控制 num 位数为 31-i,
		// 因此这句使得最高位即最右位已到最左侧且右侧空位补 0
		highBitReverse := temp << (31 - i)

		rev = rev | highBitReverse
		num = num >> 1
	}
	return rev
}

//leetcode submit region end(Prohibit modification and deletion)
