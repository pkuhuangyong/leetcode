package main

//leetcode submit region begin(Prohibit modification and deletion)
func hammingWeight(n int) int {
	cnt := 0
	for n != 0 {
		n = n & (n - 1)
		cnt++
	}
	return cnt
}

//leetcode submit region end(Prohibit modification and deletion)
