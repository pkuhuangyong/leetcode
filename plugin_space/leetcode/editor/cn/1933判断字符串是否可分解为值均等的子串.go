package main

//leetcode submit region begin(Prohibit modification and deletion)
func isDecomposable(s string) bool {
	cnt := 0
	sub := make([]int32, 0)

	for _, c := range s {
		if len(sub) == 0 || sub[0] == c {
			sub = append(sub, c)
			continue
		}

		if !checkPass(&sub, c, &cnt) {
			return false
		}
	}
	return checkPass(&sub, -1, &cnt)
}

func checkPass(sub *[]int32, c int32, cnt *int) bool {
	l := len(*sub) % 3
	if l == 1 {
		return false
	}
	if l == 2 {
		*cnt++
		if *cnt > 1 {
			return false
		}
	}

	if c == -1 {
		if *cnt != 1 {
			return false
		}
	}

	*sub = (*sub)[:0]
	*sub = append(*sub, c)
	return true
}

//leetcode submit region end(Prohibit modification and deletion)
