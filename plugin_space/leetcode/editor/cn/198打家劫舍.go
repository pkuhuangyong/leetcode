package main

//leetcode submit region begin(Prohibit modification and deletion)
func rob(nums []int) int {

	if len(nums) == 1 {
		return nums[0]
	} else {
		dp := make([]int, len(nums))
		dp[0] = nums[0]
		if nums[0] > nums[1] {
			dp[1] = nums[0]
		} else {
			dp[1] = nums[1]
		}

		for i := 2; i < len(nums); i++ {
			if dp[i-2]+nums[i] <= dp[i-1] {
				dp[i] = dp[i-1]
			} else {
				dp[i] = dp[i-2] + nums[i]
			}
		}

		return dp[len(nums)-1]
	}
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	nums := []int{2, 7, 9, 3, 1}
	// dp={2,7,11,10,12}
	rob(nums)
}
