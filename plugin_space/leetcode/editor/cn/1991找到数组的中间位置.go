package main

//leetcode submit region begin(Prohibit modification and deletion)
func findMiddleIndex(nums []int) int {
	l := len(nums)
	left := make([]int, l)
	right := make([]int, l)
	left[0] = 0
	right[l-1] = 0

	for i := 1; i < l; i++ {
		left[i] = left[i-1] + nums[i-1]
		right[l-1-i] = right[l-i] + nums[l-i]
	}

	for i := 0; i < l; i++ {
		if left[i] == right[i] {
			return i
		}
	}
	return -1
}

//leetcode submit region end(Prohibit modification and deletion)
