package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func removeNthFromEnd(head *ListNode, n int) *ListNode {

	dummy := &ListNode{0, head}
	slow, fast := dummy, head
	for n > 0 {
		fast = fast.Next
		n--
	}

	for ; fast != nil; fast = fast.Next {
		slow = slow.Next
	}

	slow.Next = slow.Next.Next
	return dummy.Next
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	node5 := &ListNode{5, nil}
	node4 := &ListNode{4, node5}
	node3 := &ListNode{3, node4}
	node2 := &ListNode{2, node3}
	head := &ListNode{1, node2}
	removeNthFromEnd(head, 2)
}

type ListNode struct {
	Val  int
	Next *ListNode
}
