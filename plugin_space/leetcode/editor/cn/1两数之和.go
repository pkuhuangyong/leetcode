package main

//leetcode submit region begin(Prohibit modification and deletion)
func twoSum(nums []int, target int) []int {
	cntMap := make(map[int]int)
	idxMap := make(map[int][]int)

	for i, num := range nums {
		cntMap[num] = cntMap[num] + 1
		idxMap[num] = append(idxMap[num], i)
	}

	res := make([]int, 0)
	for i, num := range nums {
		diff := target - num
		if cnt, ok := cntMap[diff]; ok {
			if diff == num {
				if cnt >= 2 {
					return idxMap[diff]
				}
			} else {
				return append(idxMap[diff], i)
			}
		}
	}
	return res
}

//leetcode submit region end(Prohibit modification and deletion)

func solutionTwoSum(nums []int, target int) []int {
	tb := make(map[int]int)
	for i, n := range nums {
		diff := target - n
		if v, ok := tb[diff]; ok {
			return []int{v, i}
		}

		tb[n] = i
	}
	return nil
}
