package main

//leetcode submit region begin(Prohibit modification and deletion)
func countPrimes(n int) int {
	if n <= 2 {
		return 0
	}

	isPrime := make([]bool, n)
	for i := range isPrime {
		isPrime[i] = true
	}
	cnt := 0
	for i := 2; i < n; i++ {
		if isPrime[i] {
			cnt++
			for j := i * i; j < n; j += i {
				isPrime[j] = false
			}
		}
	}
	return cnt
}

//leetcode submit region end(Prohibit modification and deletion)
