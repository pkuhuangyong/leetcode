package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reverseList(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	left, right, next := head, head.Next, head.Next
	left.Next = nil
	for next != nil {
		next = next.Next
		right.Next = left
		left = right
		right = next
	}

	return left
}

//leetcode submit region end(Prohibit modification and deletion)
