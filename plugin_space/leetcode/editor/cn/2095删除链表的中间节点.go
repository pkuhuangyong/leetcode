package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func deleteMiddle(head *ListNode) *ListNode {

	if head.Next == nil {
		return nil
	}
	if head.Next.Next == nil {
		head.Next = nil
		return head
	}

	pre := &ListNode{
		Val:  0,
		Next: head,
	}

	slow, fast := head, head
	for fast != nil && fast.Next != nil {
		pre = pre.Next
		slow = slow.Next
		fast = fast.Next.Next
	}

	pre.Next = slow.Next
	return head
}

//leetcode submit region end(Prohibit modification and deletion)
