package main

//leetcode submit region begin(Prohibit modification and deletion)
func isValid(s string) bool {
	// 通过栈来匹配左右括号，通过 map 来快速判断当前括号是左括号还是右括号
	l := len(s)
	if l%2 == 1 {
		return false
	}

	m := map[byte]byte{
		')': '(',
		']': '[',
		'}': '{',
	}
	stack := make([]byte, 0)
	for i := 0; i < l; i++ {
		c := s[i]
		if m[c] > 0 {
			// 说明当前字符属于右括号

			if len(stack) == 0 || stack[len(stack)-1] != m[c] {
				// 栈为空或者栈顶左括号不匹配，直接返回 false
				return false
			} else {
				// 说明栈顶括号匹配，栈顶左括号出栈
				stack = stack[:len(stack)-1]
			}
		} else {
			// 左括号入栈
			stack = append(stack, c)
		}
	}

	// s 遍历完成后若栈不空说明不匹配
	return len(stack) == 0
}

//leetcode submit region end(Prohibit modification and deletion)
