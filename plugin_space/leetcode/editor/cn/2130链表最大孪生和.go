package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func pairSum(head *ListNode) int {
	vals := make([]int, 0)
	cur := head
	for cur != nil {
		vals = append(vals, cur.Val)
		cur = cur.Next
	}
	maxSum := 0
	for i := 0; i < len(vals)/2; i++ {
		if maxSum < vals[i]+vals[len(vals)-1-i] {
			maxSum = vals[i] + vals[len(vals)-1-i]
		}
	}
	return maxSum
}

//leetcode submit region end(Prohibit modification and deletion)
