package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func combinationSum3(k int, n int) [][]int {

	ans := make([][]int, 0)

	var backtrack func(comb []int, left int, start int)
	backtrack = func(comb []int, left int, start int) {
		if left < 0 {
			return
		}
		if left == 0 {
			if len(comb) == k {
				temp := make([]int, k)
				copy(temp, comb)
				ans = append(ans, temp)
			}
			return
		}

		if len(comb) >= k {
			return
		}

		for i := start; i <= 9; i++ {
			comb = append(comb, i)

			backtrack(comb, left-i, i+1)

			comb = comb[:len(comb)-1]
		}
	}
	comb := make([]int, 0)
	backtrack(comb, n, 1)

	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	k, n := 4, 1
	fmt.Println(combinationSum3(k, n))
}
