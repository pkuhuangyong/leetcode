package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func containsDuplicate(nums []int) bool {
	m := make(map[int]struct{})
	for _, num := range nums {
		if _, ok := m[num]; !ok {
			m[num] = struct{}{}
		} else {
			return true
		}
	}
	return false
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	nums := []int{1, 2, 3, 4}
	duplicate := containsDuplicate(nums)
	fmt.Println(duplicate)
}
