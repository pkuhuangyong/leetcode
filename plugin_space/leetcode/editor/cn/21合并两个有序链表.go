package main

type ListNode struct {
	Val  int
	Next *ListNode
}

//leetcode submit region begin(Prohibit modification and deletion)

func mergeTwoLists(list1 *ListNode, list2 *ListNode) *ListNode {
	if list1 == nil {
		return list2
	}
	if list2 == nil {
		return list1
	}

	var root, cur1, cur2 *ListNode
	if list1.Val <= list2.Val {
		root = list1
		cur2 = list2
	} else {
		root = list2
		cur2 = list1
	}
	cur1 = root

	for cur1.Next != nil && cur2 != nil {
		for cur1.Next != nil && cur1.Next.Val < cur2.Val {
			cur1 = cur1.Next
			continue
		}

		temp := cur1.Next
		cur1.Next = cur2
		cur2 = cur2.Next
		cur1.Next.Next = temp
		cur1 = cur1.Next
	}

	if cur1.Next == nil {
		cur1.Next = cur2
	}

	return root
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	node5 := ListNode{
		Val:  5,
		Next: nil,
	}
	node4 := ListNode{
		Val:  4,
		Next: nil,
	}
	node2 := ListNode{
		Val:  2,
		Next: &node4,
	}
	node1 := ListNode{
		Val:  1,
		Next: &node2,
	}
	list1 := &node5
	list2 := &node1

	mergeTwoLists(list1, list2)
}
