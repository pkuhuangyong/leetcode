package main

//leetcode submit region begin(Prohibit modification and deletion)
func findDifference(nums1 []int, nums2 []int) [][]int {
	answer := make([][]int, 2)
	m1 := make(map[int]struct{})
	m2 := make(map[int]struct{})
	ans1 := make(map[int]struct{})
	ans2 := make(map[int]struct{})
	for _, num := range nums1 {
		m1[num] = struct{}{}
	}

	for _, num := range nums2 {
		m2[num] = struct{}{}
		if _, ok := m1[num]; !ok {
			ans2[num] = struct{}{}
		}
	}
	for _, num := range nums1 {
		if _, ok := m2[num]; !ok {
			ans1[num] = struct{}{}
		}
	}

	for k, _ := range ans1 {
		answer[0] = append(answer[0], k)
	}
	for k, _ := range ans2 {
		answer[1] = append(answer[1], k)
	}
	return answer
}

//leetcode submit region end(Prohibit modification and deletion)
