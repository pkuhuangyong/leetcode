package main

//leetcode submit region begin(Prohibit modification and deletion)
func closestNode(n int, edges [][]int, query [][]int) []int {
	res := make([]int, len(query))

	if len(edges) == 0 {
		return res
	}
	tree, dist2Root := fatherMap(edges)

	for i, q := range query {
		nearest, minDist := q[2], 1001
		lca01, lca12, lca20 := getLca(tree, q[0], q[1]), getLca(tree, q[1], q[2]), getLca(tree, q[2], q[0])

		dist := distanceOf2Nodes(dist2Root, lca12, q[1], q[2])
		if minDist > dist {
			minDist = dist
			nearest = q[1]
		}
		dist = distanceOf2Nodes(dist2Root, lca20, q[0], q[2])
		if minDist > dist {
			minDist = dist
			nearest = q[0]
		}
		dist = distanceOf2Nodes(dist2Root, getLca(tree, lca01, q[2]), lca01, q[2])
		if minDist > dist {
			minDist = dist
			nearest = lca01
		}

		path := getPath(tree, lca01, q[0], q[1])
		if _, ok := path[lca12]; ok {
			dist = distanceOf2Nodes(dist2Root, getLca(tree, lca12, q[2]), lca12, q[2])
			if minDist > dist {
				minDist = dist
				nearest = lca12
			}
		}
		if _, ok := path[lca20]; ok {
			dist = distanceOf2Nodes(dist2Root, getLca(tree, lca20, q[2]), lca20, q[2])
			if minDist > dist {
				minDist = dist
				nearest = lca20
			}
		}

		res[i] = nearest
	}

	return res
}
func getOrder(edges [][]int) []int {
	var res = make([]int, 0)
	var isSet = make(map[int]bool)
	for _, edge := range edges {
		if _, ok := isSet[edge[0]]; ok {
			continue
		} else {
			isSet[edge[0]] = true
			res = append(res, edge[0])
		}
	}

	return res
}

type void struct{}

var member void

func fatherMap(edges [][]int) (map[int]int, []int) {
	var res = make(map[int]int, 0)
	var dist2Root = make([]int, 1001)
	var graph = make(map[int]map[int]void)
	for _, edge := range edges {
		u, v := edge[0], edge[1]

		if _, ok := graph[u]; !ok {
			nodeU := make(map[int]void)
			graph[u] = nodeU
		}
		if _, ok := graph[v]; !ok {
			nodeV := make(map[int]void)
			graph[v] = nodeV
		}

		graph[u][v] = member
		graph[v][u] = member
	}

	var q queue
	h := edges[0][0]
	q.Enqueue(h)
	dist2Root[h] = 0
	for !q.IsEmpty() {
		head := q.Dequeue()
		children := graph[head]
		for child := range children {
			res[child] = head
			dist2Root[child] = dist2Root[head] + 1
			delete(graph[child], head)
			q.Enqueue(child)
		}
	}

	return res, dist2Root
}

func distanceToRoot(tree map[int]int, order []int) []int {
	res := make([]int, len(tree)+1)
	isSet := make(map[int]bool)
	res[order[0]] = 0
	isSet[order[0]] = true

	for i := 1; i < len(order); i++ {
		res[order[i]] = res[tree[order[i]]] + 1
		isSet[order[i]] = true
	}

	for k, v := range tree {
		if _, ok := isSet[k]; ok {
			continue
		} else {
			res[k] = res[v] + 1
			isSet[k] = true
		}
	}

	return res
}

func getLca(tree map[int]int, node1 int, node2 int) int {
	p1, p2 := node1, node2
	for p1 != p2 {
		if _, ok := tree[p1]; ok {
			p1 = tree[p1]
		} else {
			p1 = node2
		}

		if _, ok := tree[p2]; ok {
			p2 = tree[p2]
		} else {
			p2 = node1
		}
	}
	return p1
}

func distanceOf2Nodes(dist2Root []int, lca int, node1 int, node2 int) int {
	return dist2Root[node1] + dist2Root[node2] - 2*dist2Root[lca]
}

func getPath(tree map[int]int, lca int, node1 int, node2 int) map[int]void {
	var res = make(map[int]void)

	res[lca] = member
	for node1 != lca {
		res[node1] = member
		node1 = tree[node1]
	}
	for node2 != lca {
		res[node2] = member
		node2 = tree[node2]
	}

	return res
}

type Queue interface {
	Enqueue(node int)
	Dequeue() int
	IsEmpty() bool
}

type queue []int

func (q *queue) Enqueue(node int) {
	*q = append(*q, node)
}

func (q *queue) Dequeue() int {
	node := (*q)[0]
	*q = (*q)[1:]
	return node
}

func (q *queue) IsEmpty() bool {
	return len(*q) == 0
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	edges := [][]int{
		{1, 0},
		{0, 3},
		{2, 4},
		{4, 3},
	}

	query := [][]int{
		{0, 0, 0},
		{0, 3, 2},
		{3, 0, 0},
		{4, 3, 1},
	}
	closestNode(5, edges, query)
}
