package main

import (
	"fmt"
	"sort"
)

//leetcode submit region begin(Prohibit modification and deletion)
func successfulPairs(spells []int, potions []int, success int64) []int {
	sort.Ints(potions)
	res := make([]int, len(spells))

	for i := 0; i < len(spells); i++ {
		target := (int(success)+spells[i]-1)/spells[i] - 1
		idx := bSearch(target, 0, len(potions)-1, potions)
		res[i] = len(potions) - idx
	}
	return res
}

func bSearch(target, lo, hi int, nums []int) int {
	res := hi + 1
	for lo <= hi {
		mid := lo + (hi-lo)/2
		if nums[mid] > target {
			res = mid
			hi = mid - 1
		} else {
			lo = mid + 1
		}
	}
	return res
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	spells := []int{5, 1, 3}
	potions := []int{1, 2, 2, 2, 3, 4, 5, 6, 7, 8, 9, 10, 40}
	var success int64 = 7
	fmt.Println(bSearch(2, 0, len(potions)-1, potions))
	fmt.Println(successfulPairs(spells, potions, success))
}
