package main

//leetcode submit region begin(Prohibit modification and deletion)
func isPowerOfTwo(n int) bool {
	if n < 1 {
		return false
	}
	return n&(n-1) == 0
}

//leetcode submit region end(Prohibit modification and deletion)
