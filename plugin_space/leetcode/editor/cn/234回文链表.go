package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func isPalindrome(head *ListNode) bool {
	if head == nil || head.Next == nil {
		return true
	}
	rev := reverseList(findMidOrMidPre(head).Next)
	l1, l2 := head, rev
	for l2 != nil {
		if l1.Val != l2.Val {
			return false
		}
		l1 = l1.Next
		l2 = l2.Next
	}
	return true
}

func reverseList(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	left, right, next := head, head.Next, head.Next
	left.Next = nil
	for next != nil {
		next = next.Next
		right.Next = left
		left = right
		right = next
	}
	return left
}
func findMidOrMidPre(head *ListNode) *ListNode {
	slow, fast := head, head
	for fast.Next != nil && fast.Next.Next != nil {
		fast = fast.Next.Next
		slow = slow.Next
	}
	return slow
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	node3 := &ListNode{1, nil}
	node2 := &ListNode{2, node3}
	node1 := &ListNode{1, node2}
	head := &ListNode{1, node1}
	isPalindrome(head)
}

type ListNode struct {
	Val  int
	Next *ListNode
}
