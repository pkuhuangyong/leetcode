package main

import (
	"strconv"
)

//leetcode submit region begin(Prohibit modification and deletion)
func equalPairs(grid [][]int) int {
	ans := 0
	rowMap := make(map[string]int)
	for _, row := range grid {
		rowStr := "-"
		for _, num := range row {
			rowStr += strconv.Itoa(num) + "-"
		}
		rowMap[rowStr] += 1
	}
	for i := 0; i < len(grid); i++ {
		colStr := "-"
		for j := 0; j < len(grid); j++ {
			colStr += strconv.Itoa(grid[j][i]) + "-"
		}
		if rowMap[colStr] > 0 {
			ans += rowMap[colStr]
		}
	}
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
