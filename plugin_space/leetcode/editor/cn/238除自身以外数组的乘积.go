package main

//leetcode submit region begin(Prohibit modification and deletion)
func productExceptSelf(nums []int) []int {
	// left[i] 表示下标 i 左侧所有元素的乘积
	// right[i] 表示下标 i 右侧所有元素的乘积
	// ans[i] 表示除了下标 i 之外的所有元素的乘积
	// 很显然 ans[i]=left[i]*right[i]
	l := len(nums)
	ans := make([]int, l)
	left, right := make([]int, l), make([]int, l)
	left[0] = 1
	right[l-1] = 1

	for i := 1; i < l; i++ {
		left[i] = nums[i-1] * left[i-1]
	}
	for i := l - 2; i >= 0; i-- {
		right[i] = nums[i+1] * right[i+1]
	}
	for i := 0; i < l; i++ {
		ans[i] = left[i] * right[i]
	}
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
