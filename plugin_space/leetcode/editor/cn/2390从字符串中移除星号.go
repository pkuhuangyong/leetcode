package main

//leetcode submit region begin(Prohibit modification and deletion)
func removeStars(s string) string {
	stack := make([]byte, 0)
	for i := 0; i < len(s); i++ {
		if s[i] == '*' {
			stack = stack[:len(stack)-1]
		} else {
			stack = append(stack, s[i])
		}
	}
	return string(stack)
}

//leetcode submit region end(Prohibit modification and deletion)
