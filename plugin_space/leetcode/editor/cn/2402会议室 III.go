package main

import (
	"fmt"
	"sort"
)

//leetcode submit region begin(Prohibit modification and deletion)
func mostBooked(n int, meetings [][]int) int {
	if n == 1 {
		return 0
	}
	sort.Slice(meetings, func(i, j int) bool {
		return meetings[i][0] < meetings[j][0]
	})
	rooms := make([][]int, n)
	for i := range rooms {
		rooms[i] = []int{0, 0}
	}
	for _, meeting := range meetings {
		room := findFreeRoom(rooms, meeting[0])
		rooms[room][0] = rooms[room][0] + 1
		if meeting[0] < rooms[room][1] {
			rooms[room][1] = meeting[1] - meeting[0] + rooms[room][1]
		} else {
			rooms[room][1] = meeting[1]
		}
	}

	busiestRoom := 0
	maxTime := -1
	for i, room := range rooms {
		if maxTime < room[0] {
			maxTime = room[0]
			busiestRoom = i
		}
	}
	return busiestRoom
}

func findFreeRoom(rooms [][]int, start int) int {
	minEndTime := rooms[0][1]
	freeRoom := 0
	i := 0
	for i < len(rooms) {
		if start >= rooms[i][1] {
			freeRoom = i
			return freeRoom
		}

		if minEndTime > rooms[i][1] {
			minEndTime = rooms[i][1]
			freeRoom = i
		}
		i++
	}
	return freeRoom
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	meetings := [][]int{
		{18, 19},
		{3, 12},
		{17, 19},
		{2, 13},
		{7, 10},
	}

	fmt.Println(mostBooked(4, meetings))
}
