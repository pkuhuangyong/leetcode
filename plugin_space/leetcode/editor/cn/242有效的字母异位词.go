package main

//leetcode submit region begin(Prohibit modification and deletion)
func isAnagram(s string, t string) bool {
	if len(s) != len(t) {
		return false
	}

	var res uint8
	for i := 0; i < len(s); i++ {
		res ^= s[i]
		res ^= t[i]
	}

	return res == 0
}

//leetcode submit region end(Prohibit modification and deletion)
