package main

//leetcode submit region begin(Prohibit modification and deletion)
func shortestDistance(wordsDict []string, word1 string, word2 string) int {
	res := len(wordsDict)
	idx1, idx2 := -1, -1
	for i, w := range wordsDict {
		if w == word1 {
			idx1 = i
			if idx2 >= 0 {
				res = min(res, abs(idx1-idx2))
			}
		}
		if w == word2 {
			idx2 = i
			if idx1 >= 0 {
				res = min(res, abs(idx1-idx2))
			}
		}
	}
	return res
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func min(a, b int) int {
	if a > b {
		return b
	}
	return a
}

//leetcode submit region end(Prohibit modification and deletion)
