package main

//leetcode submit region begin(Prohibit modification and deletion)
type WordDistance struct {
	list  []string
	index map[string][]int
}

func Constructor(wordsDict []string) WordDistance {
	wd := WordDistance{
		list:  wordsDict,
		index: nil,
	}

	index := make(map[string][]int)
	for i, word := range wordsDict {
		if _, ok := index[word]; ok {
			index[word] = append(index[word], i)
		} else {
			index[word] = []int{i}
		}
	}

	wd.index = index
	return wd
}

func (this *WordDistance) Shortest(word1 string, word2 string) int {
	minD := len(this.list)
	w1Indices := this.index[word1]
	w2Indices := this.index[word2]
	i, l1 := 0, len(w1Indices)
	j, l2 := 0, len(w2Indices)

	for i < l1 && j < l2 {
		idx1, idx2 := w1Indices[i], w2Indices[j]
		if idx1 < idx2 {
			minD = minDist(minD, absDist(idx1, idx2))
			i++
		}

		if idx1 > idx2 {
			minD = minDist(minD, absDist(idx1, idx2))
			j++
		}
	}

	return minD
}

func absDist(a int, b int) int {
	if a-b < 0 {
		return b - a
	}
	return a - b
}
func minDist(a int, b int) int {
	if a > b {
		return b
	}
	return a
}

/**
 * Your WordDistance object will be instantiated and called as such:
 * obj := Constructor(wordsDict);
 * param_1 := obj.Shortest(word1,word2);
 */
//leetcode submit region end(Prohibit modification and deletion)

func main() {
	wordDict := []string{"practice", "makes", "perfect", "coding", "makes"}
	wd := Constructor(wordDict)
	wd.Shortest("coding", "practice")
}
