package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
type Vector2D struct {
	vec  [][]int
	i, j int
}

func Constructor(vec [][]int) Vector2D {
	v2d := Vector2D{
		vec: vec,
		i:   0,
		j:   0,
	}

	return v2d
}

func (this *Vector2D) Next() int {
	this.toNextValue()

	i, j := this.i, this.j
	next := this.vec[i][j]
	this.j = j + 1
	return next
}

func (this *Vector2D) HasNext() bool {
	this.toNextValue()
	return this.i < len(this.vec)
}

func (this *Vector2D) toNextValue() {
	i, j := this.i, this.j
	for i < len(this.vec) && j == len(this.vec[i]) {
		j = 0
		i++
	}
	this.i = i
	this.j = j
}

/**
 * Your Vector2D object will be instantiated and called as such:
 * obj := Constructor(vec);
 * param_1 := obj.Next();
 * param_2 := obj.HasNext();
 */
//leetcode submit region end(Prohibit modification and deletion)

func main() {
	vec := [][]int{
		{1, 2},
		{3},
		{4},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
	}
	obj := Constructor(vec)
	fmt.Println(
		obj.Next(),
		obj.Next(),
		obj.Next(),
		obj.HasNext(),
		obj.HasNext(),
		obj.Next(),
		obj.HasNext())

}
