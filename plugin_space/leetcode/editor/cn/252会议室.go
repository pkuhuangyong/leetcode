package main

import "sort"

//leetcode submit region begin(Prohibit modification and deletion)
func canAttendMeetings(intervals [][]int) bool {
	if len(intervals) < 2 {
		return true
	}

	sort.Slice(intervals, func(i, j int) bool {
		return intervals[i][0] <= intervals[j][0]
	})

	for i := 0; i+1 < len(intervals); i++ {
		if intervals[i][1] > intervals[i+1][0] {
			return false
		}
	}
	return true
}

//leetcode submit region end(Prohibit modification and deletion)
type bitmaper interface {
	Set(index int)
	Get(index int) int
}

type bitmap []uint64

func (b bitmap) Get(index int) int {
	sliceIdx := index / 64
	bitIdx := index % 64

	res := (b[sliceIdx] >> bitIdx) & 1
	return res
}

func (b bitmap) Set(index int) {
	sliceIdx := index / 64
	bitIdx := index % 64
	b[sliceIdx] |= 1 << bitIdx
}

func (b bitmap) SetInterval(l int, r int) {
	for num := l; num <= r; num++ {
		b.Set(num)
	}
}
