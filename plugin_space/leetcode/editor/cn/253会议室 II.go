package main

//leetcode submit region begin(Prohibit modification and deletion)
func minMeetingRooms(intervals [][]int) int {
	count := make([]int, 1e6+1)
	maxRoom := 0
	for _, interval := range intervals {
		count[interval[0]]++
		count[interval[1]]--
	}
	meetings := 0
	for _, s := range count {
		meetings += s
		if maxRoom < meetings {
			maxRoom = meetings
		}
	}
	return maxRoom
}

//leetcode submit region end(Prohibit modification and deletion)
