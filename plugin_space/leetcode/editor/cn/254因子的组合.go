package main

import "math"

//leetcode submit region begin(Prohibit modification and deletion)
func getFactors(n int) [][]int {

	return dfsRecur(n, 2)
}

func dfsRecur(n int, factorLimit int) [][]int {
	res := make([][]int, 0)
	sqr := int(math.Sqrt(float64(n)))
	for i := factorLimit; i <= sqr; i++ {
		if n%i == 0 {
			res = append(res, []int{i, n / i})
			for _, r := range dfsRecur(n/i, i) {
				temp := []int{i}
				temp = append(temp, r...)
				res = append(res, temp)
			}
		}
	}

	return res
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	getFactors(12)
}
