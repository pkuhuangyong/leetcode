package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func missingNumber(nums []int) int {
	n := len(nums)
	var xor int
	for i := 0; i < n; i++ {
		xor ^= nums[i]
		xor ^= i
	}
	xor ^= n
	return xor
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	nums := []int{3, 0, 1}
	fmt.Println(missingNumber(nums))
}
