package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func removeDuplicates(nums []int) int {
	// 数组 非严格递增，维护一个指针 t 指向新数组末尾，也就是新数组的最大值
	// 另一个指针 n 向后遍历原数组，如果 arr[n]>arr[t], 那么就将 arr[t+1] 与 arr[n] 交换
	// 然后 t++，直到 n 指向最后一个元素，新数组即为 arr[:t+1]，返回值为 t+1

	t := 0
	for i, num := range nums {
		if nums[i] > nums[t] {
			t++
			nums[t], num = num, nums[t]
		}
	}
	fmt.Println(t+1, nums)
	return t + 1
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	nums := []int{0, 1, 2, 3, 4}
	removeDuplicates(nums)
}
