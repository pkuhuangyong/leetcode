package main

//leetcode submit region begin(Prohibit modification and deletion)
func numWays(n int, k int) int {
	if n == 1 {
		return k
	}
	if n == 2 {
		return k * k
	}

	dp := make([][]int, n+1)
	dp[0] = []int{0, 0}
	dp[1] = []int{0, k}
	dp[2] = []int{k, k*k - k}

	for i := 3; i <= n; i++ {
		dp[i] = []int{dp[i-1][1], (k - 1) * (dp[i-1][0] + dp[i-1][1])}
		dp[0][0] = dp[i][0]
		dp[0][1] = dp[i][1]
	}

	return dp[0][0] + dp[0][1]

}

//leetcode submit region end(Prohibit modification and deletion)
