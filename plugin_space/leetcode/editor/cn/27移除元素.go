package main

//leetcode submit region begin(Prohibit modification and deletion)
func removeElement(nums []int, val int) int {
	if len(nums) == 0 {
		return 0
	}

	left, right := 0, 0
	for right < len(nums) {
		if nums[right] == val {
			right++
		} else {
			nums[left] = nums[right]
			left++
			right++
		}
	}

	return left
}

//leetcode submit region end(Prohibit modification and deletion)
