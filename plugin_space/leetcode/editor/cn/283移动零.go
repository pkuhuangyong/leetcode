package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func moveZeroes(nums []int) {
	i, j := 0, 0
	for j < len(nums) {
		if nums[j] != 0 {
			nums[i] = nums[j]
			i++
		}
		j++
	}
	for i < len(nums) {
		nums[i] = 0
		i++
	}
}
func moveZeroes1(nums []int) {

	p := 0

	for _, num := range nums {
		if num != 0 {
			nums[p] = num
			p++
		}
	}

	for ; p < len(nums); p++ {
		nums[p] = 0
	}

}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	nums := []int{0, 1, 0, 3, 12}
	fmt.Println(nums)
	moveZeroes(nums)
	fmt.Println(nums)
}
