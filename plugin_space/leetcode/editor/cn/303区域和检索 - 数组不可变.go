package main

//leetcode submit region begin(Prohibit modification and deletion)
type NumArray struct {
	preSum []int
	arr    []int
}

func Constructor(nums []int) NumArray {
	na := NumArray{preSum: make([]int, len(nums)), arr: nums}
	na.preSum[0] = nums[0]
	for i := 1; i < len(nums); i++ {
		na.preSum[i] = na.preSum[i-1] + nums[i]
	}
	return na
}

func (this *NumArray) SumRange(left int, right int) int {
	return this.preSum[right] - this.preSum[left] + this.arr[left]
}

/**
 * Your NumArray object will be instantiated and called as such:
 * obj := Constructor(nums);
 * param_1 := obj.SumRange(left,right);
 */
//leetcode submit region end(Prohibit modification and deletion)
