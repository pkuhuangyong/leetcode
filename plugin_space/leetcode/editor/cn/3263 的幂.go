package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func isPowerOfThree(n int) bool {
	if n < 1 {
		return false
	}
	return 1162261467%n == 0
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	fmt.Println(isPowerOfThree(27))
}
