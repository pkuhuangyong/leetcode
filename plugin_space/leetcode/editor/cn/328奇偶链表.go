package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func oddEvenList(head *ListNode) *ListNode {
	if head == nil || head.Next == nil || head.Next.Next == nil {
		return head
	}

	insert := head
	pre := head.Next
	cur := pre.Next

	for cur != nil {
		next := cur.Next
		pre.Next = cur.Next // take cur

		cur.Next = insert.Next
		insert.Next = cur // insert cur

		pre = pre.Next
		insert = insert.Next

		if next != nil && next.Next != nil {
			cur = next.Next
		} else {
			return head
		}
	}

	return head
}

//leetcode submit region end(Prohibit modification and deletion)
