package main

import "math"

//leetcode submit region begin(Prohibit modification and deletion)
func increasingTriplet(nums []int) bool {
	l := len(nums)
	if l < 3 {
		return false
	}
	first := nums[0]
	second := math.MaxInt64
	for i := 0; i < len(nums); i++ {
		if nums[i] <= first {
			first = nums[i]
		} else if nums[i] <= second {
			second = nums[i]
		} else {
			return true
		}
	}
	return false
}
func increasingTriplet1(nums []int) bool {
	l := len(nums)
	if l < 3 {
		return false
	}

	leftMin, rightMax := make([]int, l), make([]int, l)
	leftMin[0] = nums[0]
	rightMax[l-1] = nums[l-1]
	for i := 1; i < l; i++ {
		leftMin[i] = min(leftMin[i-1], nums[i-1])
	}
	for j := l - 2; j > 0; j-- {
		rightMax[j] = max(rightMax[j+1], nums[j+1])
	}
	for i := 1; i < l-1; i++ {
		if leftMin[i] < nums[i] && nums[i] < rightMax[i] {
			return true
		}
	}
	return false
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}
func max(x, y int) int {
	if x < y {
		return y
	}
	return x
}

//leetcode submit region end(Prohibit modification and deletion)
