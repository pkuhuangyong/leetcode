package main

//leetcode submit region begin(Prohibit modification and deletion)
func countBits(n int) []int {
	bits := make([]int, n+1)
	bits[0] = 0
	if n == 0 {
		return bits
	}
	bits[1] = 1
	highBit := 2
	for i := 2; i <= n; i++ {
		if i&(i-1) == 0 {
			highBit = i
		}
		bits[i] = bits[i-highBit] + 1
	}
	return bits
}

//leetcode submit region end(Prohibit modification and deletion)
