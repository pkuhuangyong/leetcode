package main

//leetcode submit region begin(Prohibit modification and deletion)
func isPowerOfFour(n int) bool {
	if n < 1 {
		return false
	}

	return (n&0xaaaaaaaa == 0) && (n&(n-1) == 0)
}

//leetcode submit region end(Prohibit modification and deletion)
