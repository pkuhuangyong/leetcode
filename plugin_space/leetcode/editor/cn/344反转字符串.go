package main

//leetcode submit region begin(Prohibit modification and deletion)
func reverseString(s []byte) {
	l := len(s)
	for i := 0; i < l/2; i++ {
		s[i], s[l-i-1] = s[l-i-1], s[i]
	}
}

//leetcode submit region end(Prohibit modification and deletion)
