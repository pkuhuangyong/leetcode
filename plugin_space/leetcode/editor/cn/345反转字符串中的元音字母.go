package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func reverseVowels(s string) string {
	m := make(map[int32]struct{})
	vowels := "aeiouAEIOU"
	for _, vowel := range vowels {
		m[vowel] = struct{}{}
	}
	list := make([]int32, 0)
	for _, ch := range s {
		list = append(list, ch)
	}
	low, high := 0, len(s)-1
	for low < high {
		i := low
		for ; i < high; i++ {
			if _, ok := m[list[i]]; ok {
				break
			}
		}

		j := high
		for ; j > low; j-- {
			if _, ok := m[list[j]]; ok {
				break
			}
		}
		if i <= j {
			list[i], list[j] = list[j], list[i]
		}
		low = i + 1
		high = j - 1
	}

	ans := ""
	for _, ch := range list {
		ans += string(ch)
	}
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	s := "he"
	s1 := "leetcode"

	fmt.Println(reverseVowels(s))
	fmt.Println(reverseVowels(s1))
}
