package main

//leetcode submit region begin(Prohibit modification and deletion)
func intersect(nums1 []int, nums2 []int) []int {
	m := make(map[int]int)
	var res []int

	for _, num := range nums1 {
		if _, ok := m[num]; !ok {
			m[num] = 1
		} else {
			m[num] = m[num] + 1
		}
	}

	for _, num := range nums2 {
		if _, ok := m[num]; ok {
			res = append(res, num)

			v := m[num]
			if v == 1 {
				delete(m, num)
			} else {
				m[num] = v - 1
			}
		}
	}

	return res
}

//leetcode submit region end(Prohibit modification and deletion)
