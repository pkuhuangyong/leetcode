package main

import (
	"fmt"
	"reflect"
	"testing"
)

func Test_intersect(t *testing.T) {
	type args struct {
		nums1 []int
		nums2 []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "example 1",
			args: args{
				[]int{1, 2, 2, 1},
				[]int{2, 2},
			},
			want: []int{2, 2},
		},
		{
			name: "example 2",
			args: args{
				[]int{4, 9, 5},
				[]int{9, 4, 9, 8, 4},
			},
			want: []int{4, 9},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := intersect(tt.args.nums1, tt.args.nums2); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("intersect() = %v, want %v", got, tt.want)
			}
		})
	}
}

func intersect(nums1 []int, nums2 []int) []int {
	m := make(map[int]int)
	var res []int

	for _, num := range nums1 {
		if _, ok := m[num]; !ok {
			m[num] = 1
		} else {
			m[num] = m[num] + 1
		}
	}

	for _, num := range nums2 {
		if _, ok := m[num]; ok {
			res = append(res, num)

			v := m[num]
			if v == 1 {
				delete(m, num)
			} else {
				m[num] = v - 1
			}
		}
	}

	fmt.Println(len(res))
	return res
}
