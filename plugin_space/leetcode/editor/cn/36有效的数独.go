package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func isValidSudoku(board [][]byte) bool {
	var rows = make([]map[byte]struct{}, 9)
	var columns = make([]map[byte]struct{}, 9)
	var squares = make([][]map[byte]struct{}, 3)
	for i, _ := range squares {
		squares[i] = make([]map[byte]struct{}, 3)
	}

	for x := 0; x < 9; x++ {
		rows[x] = make(map[byte]struct{})
		columns[x] = make(map[byte]struct{})
		squares[x/3][x%3] = make(map[byte]struct{})
	}

	for i, rs := range board {
		for j, num := range rs {
			if num == '.' {
				continue
			}
			if _, ok := rows[i][num]; ok {
				return false
			} else {
				rows[i][num] = struct{}{}
			}
			if _, ok := columns[j][num]; ok {
				return false
			} else {
				columns[j][num] = struct{}{}
			}
			rIdx := i / 3
			cIdx := j / 3
			if _, ok := squares[rIdx][cIdx][num]; ok {
				return false
			} else {
				squares[rIdx][cIdx][num] = struct{}{}
			}
		}
	}
	return true
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	board := [][]byte{
		{'5', '3', '.', '.', '7', '.', '.', '.', '.'},
		{'6', '.', '.', '1', '9', '5', '.', '.', '.'},
		{'.', '9', '8', '.', '.', '.', '.', '6', '.'},
		{'8', '.', '.', '.', '6', '.', '.', '.', '3'},
		{'4', '.', '.', '8', '.', '3', '.', '.', '1'},
		{'7', '.', '.', '.', '2', '.', '.', '.', '6'},
		{'.', '6', '.', '.', '.', '.', '2', '8', '.'},
		{'.', '.', '.', '4', '1', '9', '.', '.', '5'},
		{'.', '.', '.', '.', '8', '.', '.', '7', '9'},
	}

	fmt.Println(isValidSudoku(board))
}
