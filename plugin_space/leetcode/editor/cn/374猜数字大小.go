package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Forward declaration of guess API.
 * @param  num   your guess
 * @return 	     -1 if num is higher than the picked number
 *			      1 if num is lower than the picked number
 *               otherwise return 0
 * func guess(num int) int;
 */

func guessNumber(n int) int {
	return g(1, n)
}

func g(low, high int) int {
	mid := low + (high-low)/2
	if guess(mid) == 0 {
		return mid
	}
	if guess(mid) == -1 {
		return g(low, mid)
	}
	if guess(mid) == 1 {
		return g(mid+1, high)
	}
	return 0
}

//leetcode submit region end(Prohibit modification and deletion)
func guess(num int) int {
	return 0
}
