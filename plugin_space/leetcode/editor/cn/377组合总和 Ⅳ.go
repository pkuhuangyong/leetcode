package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func combinationSum4(nums []int, target int) int {

	dp := make([]int, target+1)
	dp[0] = 1

	for t := 1; t <= target; t++ {
		tSumCount := 0

		for i := 0; i < len(nums); i++ {
			preTarget := t - nums[i]
			if preTarget >= 0 {
				tSumCount += dp[preTarget]
			}
		}
		dp[t] = tSumCount
	}

	return dp[target]
}
func combinationSum40(nums []int, target int) int {
	count := 0

	var backtrack func(left int)
	backtrack = func(left int) {
		if left < 0 {
			return
		}
		if left == 0 {
			count++
			return
		}
		for i := 0; i < len(nums); i++ {
			backtrack(left - nums[i])
		}
	}

	backtrack(target)
	return count
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	nums := []int{1, 2, 3}
	target := 32
	fmt.Println(combinationSum4(nums, target))
}
