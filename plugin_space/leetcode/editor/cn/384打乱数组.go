package main

import "math/rand"

//leetcode submit region begin(Prohibit modification and deletion)
type Solution struct {
	nums []int
}

func Constructor(nums []int) Solution {
	s := Solution{nums: nums}
	return s
}

func (this *Solution) Reset() []int {
	n := this.nums
	return n
}

func (this *Solution) Shuffle() []int {
	temp := make([]int, len(this.nums))
	copy(temp, this.nums)
	for i := 0; i < len(temp); i++ {
		idx := rand.Intn(len(temp)-i) + i
		temp[idx], temp[i] = temp[i], temp[idx]
	}
	return temp
}

/**
 * Your Solution object will be instantiated and called as such:
 * obj := Constructor(nums);
 * param_1 := obj.Reset();
 * param_2 := obj.Shuffle();
 */
//leetcode submit region end(Prohibit modification and deletion)
