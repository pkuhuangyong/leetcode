package main

//leetcode submit region begin(Prohibit modification and deletion)
func firstUniqChar(s string) int {
	m := make(map[int32]int)

	for _, char := range s {
		if _, ok := m[char]; !ok {
			m[char] = 1
		} else {
			m[char] = m[char] + 1
		}
	}

	for i, char := range s {
		if m[char] == 1 {
			return i
		}
	}

	return -1
}

//leetcode submit region end(Prohibit modification and deletion)
