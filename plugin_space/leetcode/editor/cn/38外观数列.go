package main

import (
	"fmt"
	"strconv"
)

//leetcode submit region begin(Prohibit modification and deletion)
func countAndSay(n int) string {
	if n == 1 {
		return "1"
	}

	s := countAndSay(n - 1)
	res := ""
	i, c := 1, 1
	cur := s[0]
	for ; i < len(s); i++ {
		if s[i] == cur {
			c++
		} else {
			res = res + strconv.Itoa(c) + string(cur)
			cur = s[i]
			c = 1
		}
	}
	res = res + strconv.Itoa(c) + string(cur)

	return res
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	fmt.Println(countAndSay(4))
}
