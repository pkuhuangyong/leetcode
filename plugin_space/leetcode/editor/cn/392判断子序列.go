package main

import (
	"strings"
)

//leetcode submit region begin(Prohibit modification and deletion)
func isSubsequence(s string, t string) bool {
	if len(s) == 0 {
		return true
	}
	if len(t) == 0 {
		return false
	}
	if len(s) == len(t) {
		return s == t
	}
	if len(s) > len(t) {
		return false
	}
	dp := make([][]bool, len(t))
	for i := 0; i < len(dp); i++ {
		temp := make([]bool, len(s))
		dp[i] = temp
	}
	for i := 0; i < len(dp); i++ {
		dp[i][0] = strings.ContainsRune(t[:i+1], rune(s[0]))
	}
	for i := 0; i < len(t); i++ {
		for j := 1; j < len(s); j++ {
			if j > i {
				dp[i][j] = false
			} else {
				if t[i] == s[j] {
					dp[i][j] = dp[i-1][j-1]
				} else {
					dp[i][j] = dp[i-1][j]
				}
			}
		}
	}
	return dp[len(t)-1][len(s)-1]
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	s := "axc"
	t := "ahbgdc"

	isSubsequence(s, t)
	/**
	[true false false]
	[true true false]
	[true true true]
	[true true true]
	[true true true]
	[true true true]
	[true false]
	[true false]
	[true true]
	[true true]
	[true true]
	[true true]
	*/
}
