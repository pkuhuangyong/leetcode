package main

import (
	"strconv"
	"unicode"
)

//leetcode submit region begin(Prohibit modification and deletion)
func decodeString(s string) string {
	var (
		stk []rune
	)

	for _, c := range s {
		if c != ']' {
			stk = append(stk, c)
			continue
		}

		//计算str
		var j int
		for j = len(stk) - 1; stk[j] != '['; j-- {
		}
		repStr := slices.Clone(stk[j+1:]) // 注意此处要clone，否则会报错 因为go底层slices共享内存原理等等好像
		stk = stk[:len(stk)-len(repStr)-1]

		//num
		for j = len(stk) - 1; j >= 0 && unicode.IsDigit(stk[j]); j-- {
		}
		num, _ := strconv.Atoi(string(stk[j+1:]))
		stk = stk[:j+1]

		//rep
		for i := 0; i < num; i++ {
			stk = append(stk, repStr...)
		}
	}

	return string(stk)
}

//leetcode submit region end(Prohibit modification and deletion)
