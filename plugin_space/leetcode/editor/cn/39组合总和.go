package main

import (
	"fmt"
	"sort"
)

//leetcode submit region begin(Prohibit modification and deletion)
func combinationSum(candidates []int, target int) [][]int {
	ans := make([][]int, 0)
	sort.Ints(candidates)
	var backtrack func(selected []int, left int, start int)
	backtrack = func(selected []int, left int, start int) {
		if left < 0 {
			return
		}
		if left == 0 {
			temp := make([]int, len(selected))
			copy(temp, selected)
			ans = append(ans, temp)
			return
		}

		for i := start; i < len(candidates); i++ {
			selected = append(selected, candidates[i])
			backtrack(selected, left-candidates[i], i)
			selected = selected[:len(selected)-1]
		}
	}

	selected := make([]int, 0)
	backtrack(selected, target, 0)
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	candidates := []int{2, 3, 5}
	target := 8
	fmt.Println(combinationSum(candidates, target))
}
