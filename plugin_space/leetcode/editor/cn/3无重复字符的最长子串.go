package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func lengthOfLongestSubstring(s string) int {
	check := quickCheck(s)
	if check != -1 {
		return check
	}

	var charSet set = make(map[uint8]v)
	slow, fast := 0, 0
	max := 0

	for fast < len(s) {
		if charSet.Contains(s[fast]) {
			if max < fast-slow {
				max = fast - slow
			}

			for charSet.Contains(s[fast]) && slow < fast {
				charSet.Remove(s[slow])
				slow++
			}
		}

		charSet.Add(s[fast])
		fast++
	}

	if max < fast-slow {
		max = fast - slow
	}

	return max
}

type v struct {
}

var null = v{}

type set map[uint8]v

func (s set) Add(byt uint8) {
	s[byt] = null
}
func (s set) Remove(byt uint8) {
	delete(s, byt)
}
func (s set) Contains(byt uint8) bool {
	_, ok := s[byt]
	return ok
}

func quickCheck(s string) int {
	if len(s) == 0 {
		return 0
	} else if len(s) == 1 {
		return 1
	} else if len(s) == 2 {
		if s[0] == s[1] {
			return 1
		} else {
			return 2
		}
	} else {
		return -1
	}
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	s := "abcabcbb"
	fmt.Println(lengthOfLongestSubstring(s))
}
