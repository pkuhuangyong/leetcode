package main

import (
	"fmt"
	"sort"
)

//leetcode submit region begin(Prohibit modification and deletion)
func combinationSum2(candidates []int, target int) [][]int {
	ans := make([][]int, 0)
	sort.Ints(candidates)
	used := make([]bool, len(candidates))
	var backtrack func(selected []int, left int, start int, used []bool)
	backtrack = func(selected []int, left int, start int, used []bool) {
		if left < 0 {
			return
		}
		if left == 0 {
			temp := make([]int, len(selected))
			copy(temp, selected)
			ans = append(ans, temp)
			return
		}

		for i := start; i < len(candidates); i++ {
			if i > 0 && candidates[i] == candidates[i-1] && !used[i-1] {
				continue
			}
			selected = append(selected, candidates[i])
			used[i] = true
			backtrack(selected, left-candidates[i], i+1, used)
			selected = selected[:len(selected)-1]
			used[i] = false
		}
	}
	selected := make([]int, 0)
	backtrack(selected, target, 0, used)
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	candidates := []int{2, 5, 2, 1, 2}
	target := 5
	fmt.Println(combinationSum2(candidates, target))
}
