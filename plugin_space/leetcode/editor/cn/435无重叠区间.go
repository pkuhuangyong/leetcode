package main

import "sort"

//leetcode submit region begin(Prohibit modification and deletion)
func eraseOverlapIntervals(intervals [][]int) int {
	sort.Slice(intervals, func(i, j int) bool {
		return intervals[i][1] < intervals[j][1]
	})

	rmCount := 0
	end := intervals[0][1]
	for i := 1; i < len(intervals); i++ {
		if intervals[i][0] < end {
			rmCount++
		} else {
			end = intervals[i][1]
		}
	}
	return rmCount
}

//leetcode submit region end(Prohibit modification and deletion)
