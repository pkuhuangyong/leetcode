package main

import "strconv"

//leetcode submit region begin(Prohibit modification and deletion)
func compress(chars []byte) int {
	count := 0
	prev := chars[0]
	idx := 0
	for i := 0; i < len(chars); i++ {
		if chars[i] == prev {
			count++
		} else {
			chars[idx] = prev
			idx++
			if count > 1 {
				toBytes := iToBytes(count)
				for j := 0; j < len(toBytes); j++ {
					chars[idx+j] = toBytes[j]
				}
				idx += len(toBytes)
			}
			prev = chars[i]
			count = 1
		}
	}

	chars[idx] = prev
	idx++
	if count > 1 {
		toBytes := iToBytes(count)
		for j := 0; j < len(toBytes); j++ {
			chars[idx+j] = toBytes[j]
		}
		idx += len(toBytes)
	}
	return idx
}
func iToBytes(num int) []byte {
	ans := make([]byte, 0)
	a := strconv.Itoa(num)
	for i := 0; i < len(a); i++ {
		ans = append(ans, a[i])
	}
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
