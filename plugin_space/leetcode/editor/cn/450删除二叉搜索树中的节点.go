package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func deleteNode(root *TreeNode, key int) *TreeNode {
	if root == nil {
		return nil
	}
	if root.Val == key {
		if root.Left == nil {
			return root.Right
		}
		if root.Right == nil {
			return root.Left
		}

		rightMinNode := getMinNode(root.Right)

		rightMinNode.Right = deleteNode(root.Right, rightMinNode.Val)
		rightMinNode.Left = root.Left
		root = rightMinNode

	} else if key > root.Val {
		root.Right = deleteNode(root.Right, key)
	} else {
		root.Left = deleteNode(root.Left, key)
	}
	return root
}

func getMinNode(root *TreeNode) *TreeNode {
	if root.Left == nil {
		return root
	}
	return getMinNode(root.Left)
}

//leetcode submit region end(Prohibit modification and deletion)
