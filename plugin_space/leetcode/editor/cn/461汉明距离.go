package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func hammingDistance(x int, y int) int {
	ham := x ^ y
	return hammingWeight(ham)
}

func hammingWeight(n int) int {
	cnt := 0
	for n != 0 {
		n = n & (n - 1)
		cnt++
	}
	return cnt
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	fmt.Println(hammingDistance(3, 1))
}
