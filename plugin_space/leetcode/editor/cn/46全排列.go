package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func permute(nums []int) [][]int {
	ans := make([][]int, 0)

	var backtrack func(route []int, used []bool)
	used := make([]bool, len(nums))
	for i := 0; i < len(used); i++ {
		used[i] = false
	}

	backtrack = func(path []int, used []bool) {
		if len(nums) == len(path) {
			temp := make([]int, len(nums))
			copy(temp, path)
			ans = append(ans, temp)
			return
		}
		for i := 0; i < len(nums); i++ {
			if used[i] {
				continue
			}

			path = append(path, nums[i])
			used[i] = true

			backtrack(path, used)

			path = path[0 : len(path)-1]
			used[i] = false

		}
	}
	path := make([]int, 0)
	backtrack(path, used)
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	nums := []int{5, 4, 6, 2}
	fmt.Println(permute(nums))
}
