package main

import (
	"fmt"
	"sort"
	"strconv"
)

//leetcode submit region begin(Prohibit modification and deletion)
func permuteUnique(nums []int) [][]int {
	sort.Ints(nums)
	ans := make([][]int, 0)
	used := make([]bool, len(nums))
	var backtrack func(path []int, used []bool)
	backtrack = func(path []int, used []bool) {
		if len(nums) == len(path) {
			temp := make([]int, len(nums))
			copy(temp, path)
			ans = append(ans, temp)
		}

		for i := 0; i < len(nums); i++ {
			if i > 0 && nums[i] == nums[i-1] && !used[i-1] {
				continue
			}
			if used[i] {
				continue
			}

			path = append(path, nums[i])
			used[i] = true

			backtrack(path, used)

			path = path[:len(path)-1]
			used[i] = false
		}
	}
	path := make([]int, 0)
	backtrack(path, used)
	return ans
}
func permuteUnique1(nums []int) [][]int {
	m := make(map[string][]int)
	used := make([]bool, len(nums))

	var backtrack func(path []int, used []bool)
	backtrack = func(path []int, used []bool) {
		if len(path) == len(nums) {
			temp := make([]int, len(nums))
			copy(temp, path)
			k := getLiteral(temp)
			m[k] = temp
			return
		}

		for i := 0; i < len(nums); i++ {
			if used[i] {
				continue
			}

			path = append(path, nums[i])
			used[i] = true

			backtrack(path, used)

			path = path[:len(path)-1]
			used[i] = false
		}
	}

	path := make([]int, 0)
	backtrack(path, used)
	ans := make([][]int, 0)
	for _, ints := range m {
		ans = append(ans, ints)
	}
	return ans
}

func getLiteral(nums []int) string {
	res := ""
	for _, num := range nums {
		res += strconv.Itoa(num)
	}
	return res
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	nums := []int{1, 1, 2}
	fmt.Println(permuteUnique(nums))
}
