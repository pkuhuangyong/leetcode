package main

import (
	"fmt"
)

//leetcode submit region begin(Prohibit modification and deletion)
func maxSubArray(nums []int) int {
	dp := make([]int, len(nums))
	dp[0] = nums[0]
	for i := 1; i < len(nums); i++ {
		if dp[i-1]+nums[i] > nums[i] {
			dp[i] = dp[i-1] + nums[i]
		} else {
			dp[i] = nums[i]
		}
	}

	maxSum := dp[0]
	for i := 1; i < len(nums); i++ {
		if maxSum < dp[i] {
			maxSum = dp[i]
		}
	}
	return maxSum
}

//leetcode submit region end(Prohibit modification and deletion)

/**
对于长度为 n 的数组，假设 dp[n] 表示考虑其第 n 元素参与时的最大子序和：
如果第 n 个元素为正数，那么 dp[n]=dp[n-1]+4
如果第 n 个元素为负数，那么 dp[n]=dp[n-1]
*/
func main() {
	nums := []int{-2, 1, -3, 4, -1, 2, 1, -5, 4}
	fmt.Println(maxSubArray(nums))
}
