package main

//leetcode submit region begin(Prohibit modification and deletion)
func merge(intervals [][]int) [][]int {

	min, max := 10001, 0
	for _, interval := range intervals {
		if min > interval[0] {
			min = interval[0]
		}
		if max < interval[1] {
			max = interval[1]
		}
	}
	var left = make([]int, max+1)
	var right = make([]int, max+1)
	for _, interval := range intervals {
		l := interval[0]
		left[l] += 1

		r := interval[1]
		right[r] += 1
	}

	var res = make([][]int, 0)
	leftCnt := 0
	var start = 0
	for i := min; i <= max; i++ {
		if left[i] > 0 {
			leftCnt += left[i]
			if leftCnt == left[i] {
				start = i
			}
		}

		if right[i] > 0 {
			leftCnt -= right[i]
			if leftCnt == 0 {
				var temp = make([]int, 2)
				temp[0] = start
				temp[1] = i
				res = append(res, temp)
			}
		}
	}
	return res
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	var intervals = [][]int{
		//{1, 3},
		{2, 6},
		{8, 10},
		{1, 18},
	}

	merge(intervals)
}
