package main

//leetcode submit region begin(Prohibit modification and deletion)
func insert(intervals [][]int, newInterval []int) [][]int {
	if intervals == nil || len(intervals) == 0 {
		return [][]int{newInterval}
	}
	res := make([][]int, 0)
	inserted := false
	for i, interval := range intervals {
		if interval[1] < newInterval[0] {
			res = append(res, interval)
			if i == len(intervals)-1 {
				res = append(res, newInterval)
			}
		} else if interval[0] > newInterval[1] {
			if !inserted {
				res = append(res, newInterval, interval)
				inserted = true
			} else {
				res = append(res, interval)
			}
		} else {
			newInterval = mergeIntervals(interval, newInterval)
			if i == len(intervals)-1 {
				res = append(res, newInterval)
			}
		}
	}

	return res
}

func mergeIntervals(l []int, r []int) []int {

	newL := min(l[0], r[0])
	newR := max(l[1], r[1])
	return []int{newL, newR}
}

func min(a int, b int) int {
	if a-b > 0 {
		return b
	} else {
		return a
	}
}

func max(a int, b int) int {
	if a-b < 0 {
		return b
	} else {
		return a
	}
}

//leetcode submit region end(Prohibit modification and deletion)
