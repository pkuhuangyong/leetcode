package main

//leetcode submit region begin(Prohibit modification and deletion)
func killProcess(pid []int, ppid []int, kill int) []int {
	res := make([]int, 0)
	dfs(generateTree(pid, ppid), kill, &res)
	return res
}

func generateTree(pid []int, ppid []int) [][]int {
	maxPid := 0
	for _, id := range pid {
		if maxPid < id {
			maxPid = id
		}
	}

	res := make([][]int, maxPid+1)

	for i := 0; i < len(pid); i++ {
		if res[ppid[i]] == nil {
			temp := make([]int, 0)
			res[ppid[i]] = temp
		}
		res[ppid[i]] = append(res[ppid[i]], pid[i])
	}

	return res
}

func dfs(tree [][]int, root int, preOrder *[]int) {
	*preOrder = append(*preOrder, root)
	if tree[root] == nil {
		return
	}
	for _, child := range tree[root] {
		dfs(tree, child, preOrder)
	}
}

//leetcode submit region end(Prohibit modification and deletion)
