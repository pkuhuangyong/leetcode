package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func canPlaceFlowers(flowerbed []int, n int) bool {
	if n == 0 {
		return true
	}
	if n > (len(flowerbed)+1)/2 {
		return false
	}

	firstOneIdx := -1
	for i := 0; i < len(flowerbed); i++ {
		if flowerbed[i] == 1 {
			firstOneIdx = i
			break
		}
	}
	// 没有 1
	if firstOneIdx == -1 {
		return (len(flowerbed)+1)/2 >= n
	}

	lastOneIdx := -1
	for i := len(flowerbed) - 1; i >= 0; i-- {
		if flowerbed[i] == 1 {
			lastOneIdx = i
			break
		}
	}

	left := firstOneIdx / 2
	right := (len(flowerbed) - 1 - lastOneIdx) / 2
	// 只有 1 个 1
	if firstOneIdx == lastOneIdx {
		return (left + right) >= n
	}

	sum := left + right
	for i := firstOneIdx + 1; i < lastOneIdx; {
		zeroCount := 0
		for j := i; ; {
			if flowerbed[j] == 0 {
				zeroCount++
				j++
			} else {
				i = j + 1
				break
			}
		}

		sum += (zeroCount - 1) / 2
		if sum >= n {
			return true
		}
	}
	if sum >= n {
		return true
	} else {
		return false
	}
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	flowerbed := []int{1, 0, 0, 0, 1}
	n := 2
	fmt.Println(canPlaceFlowers(flowerbed, n))
}
