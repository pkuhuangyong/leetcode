package main

import (
	"fmt"
	"strconv"
)

//leetcode submit region begin(Prohibit modification and deletion)
func getPermutation(n int, k int) string {
	used := make([]bool, n+1)
	count := 0
	ans := ""
	var backtrack func(perm []int, used []bool)
	backtrack = func(perm []int, used []bool) {
		if len(perm) == n {
			count++
			if count == k {
				for _, i := range perm {
					ans += strconv.Itoa(i)
				}
			}
			return
		}

		for i := 1; i <= n; i++ {
			if used[i] {
				continue
			}

			perm = append(perm, i)
			used[i] = true

			backtrack(perm, used)

			perm = perm[:len(perm)-1]
			used[i] = false
		}
	}
	perm := make([]int, 0)
	backtrack(perm, used)
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	fmt.Println(getPermutation(4, 9))
}
