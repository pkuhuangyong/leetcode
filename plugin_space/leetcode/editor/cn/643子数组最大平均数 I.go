package main

//leetcode submit region begin(Prohibit modification and deletion)
func findMaxAverage(nums []int, k int) float64 {
	firstAverage := 0.0
	fk := float64(k)
	for i := 0; i < k; i++ {
		firstAverage += float64(nums[i]) / fk
	}
	maxAverage := firstAverage
	dp := make([]float64, len(nums))
	dp[k-1] = firstAverage
	for i := k; i < len(dp); i++ {
		dp[i] = dp[i-1] + float64(nums[i])/fk - float64(nums[i-k])/fk
		if maxAverage < dp[i] {
			maxAverage = dp[i]
		}
	}
	return maxAverage
}

//leetcode submit region end(Prohibit modification and deletion)
