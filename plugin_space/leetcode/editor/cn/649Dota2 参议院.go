package main

//leetcode submit region begin(Prohibit modification and deletion)
func predictPartyVictory(senate string) string {
	dire, radiant := make([]int, 0), make([]int, 0)
	for i := 0; i < len(senate); i++ {
		if senate[i] == 'R' {
			radiant = append(radiant, i)
		} else {
			dire = append(dire, i)
		}
	}
	for len(dire) > 0 && len(radiant) > 0 {
		if radiant[0] < dire[0] {
			radiant = append(radiant, radiant[0]+len(senate))
		} else {
			dire = append(dire, dire[0]+len(senate))
		}
		radiant = radiant[1:]
		dire = dire[1:]
	}
	if len(dire) > 0 {
		return "Dire"
	} else {
		return "Radiant"
	}
}

//leetcode submit region end(Prohibit modification and deletion)
