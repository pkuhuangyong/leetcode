package main

//leetcode submit region begin(Prohibit modification and deletion)
func pathSum(nums []int) int {
	sum := 0
	tree := sequenceTree(nums)
	countLeaf(tree)
	for _, node := range tree {
		if node != nil {
			sum += node[0] * node[1]
		}
	}
	return sum
}

func sequenceTree(nums []int) [][]int {
	res := make([][]int, 32)

	for _, num := range nums {
		h := num / 100
		levelIdx := (num % 100) / 10
		weight := (num % 100) % 10

		idx := 1<<(h-1) - 1 + levelIdx
		temp := []int{weight, 0}
		res[idx] = temp
	}

	return res
}

func countLeaf(tree [][]int) {
	for i := 31; i > 15; i-- {
		if tree[i] == nil {
			continue
		}
		tree[i][1] = 1
	}

	for i := 15; i > 0; i-- {
		if tree[i] == nil {
			continue
		}

		if tree[2*i] == nil && tree[2*i+1] == nil {
			tree[i][1] = 1
			continue
		}

		if tree[2*i] != nil {
			tree[i][1] += tree[2*i][1]
		}

		if tree[2*i+1] != nil {
			tree[i][1] += tree[2*i+1][1]
		}
	}
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	nums := []int{113, 215, 221}
	pathSum(nums)
}
