package main

//leetcode submit region begin(Prohibit modification and deletion)
func plusOne(digits []int) []int {
	plus := 1
	sum := make([]int, len(digits)+1)
	for i := len(digits) - 1; i >= 0; i-- {
		d := digits[i] + plus
		plus = d / 10
		d = d % 10
		sum[i+1] = d
	}
	if plus == 1 {
		sum[0] = 1
	} else {
		sum = sum[1:]
	}
	return sum
}

//leetcode submit region end(Prohibit modification and deletion)
