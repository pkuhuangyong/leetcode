package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func climbStairs(n int) int {
	if n == 1 {
		return 1
	} else if n == 2 {
		return 2
	}
	dp := make([]int, n+1)
	dp[0] = 1
	dp[1] = 1
	dp[2] = 2

	for i := 2; i <= n; i++ {
		dp[i] = dp[i-1] + dp[i-2]
	}

	return dp[n]
}

//leetcode submit region end(Prohibit modification and deletion)

/**
爬到楼顶有2种方式：
	1、站在倒数第一台阶上一步
	2、站在倒数第二台阶上2步

假设 dp[i] 表示上第 i 级台阶的方式总数，那么有
	dp[i]=dp[i-1]+dp[i-2]
*/

func main() {
	fmt.Println(climbStairs(2))
}
