package main

//leetcode submit region begin(Prohibit modification and deletion)
func asteroidCollision(asteroids []int) []int {
	stack := make([]int, 0)
	for _, a := range asteroids {
		alive := true
		for alive && a < 0 && len(stack) > 0 && stack[len(stack)-1] > 0 {
			alive = stack[len(stack)-1]+a < 0
			if stack[len(stack)-1]+a <= 0 {
				stack = stack[:len(stack)-1]
			}
		}
		if alive {
			stack = append(stack, a)
		}
	}
	return stack
}

//leetcode submit region end(Prohibit modification and deletion)
