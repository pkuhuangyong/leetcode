package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func combine(n int, k int) [][]int {
	ans := make([][]int, 0)

	var backtrack func(comb []int, start int)
	backtrack = func(comb []int, start int) {
		if len(comb) == k {
			temp := make([]int, k)
			copy(temp, comb)
			ans = append(ans, temp)
			return
		}

		for i := start; i <= n; i++ {
			comb = append(comb, i)
			backtrack(comb, i+1)
			comb = comb[:len(comb)-1]
		}
	}
	comb := make([]int, 0)
	backtrack(comb, 1)
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	n := 4
	k := 3
	fmt.Println(combine(n, k))
}
