package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func subsets(nums []int) [][]int {
	ans := make([][]int, 0)
	ans = append(ans, []int{})
	var backtrack func(subSet []int, length int, start int)
	backtrack = func(subSet []int, length int, start int) {
		if len(subSet) == length {
			temp := make([]int, length)
			copy(temp, subSet)
			ans = append(ans, temp)
			return
		}

		for i := start; i < len(nums); i++ {

			subSet = append(subSet, nums[i])
			backtrack(subSet, length, i+1)
			subSet = subSet[:len(subSet)-1]
		}
	}

	for l := 1; l <= len(nums); l++ {
		subSet := make([]int, 0)
		backtrack(subSet, l, 0)
	}

	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	nums := []int{1, 2, 3}
	fmt.Println(subsets(nums))
}
