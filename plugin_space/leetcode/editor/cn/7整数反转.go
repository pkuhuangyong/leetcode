package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func reverse(x int) int {
	y := 0
	for x != 0 {
		y = y*10 + x%10
		if !(-(1<<31) <= y && y <= (1<<31)-1) {
			return 0
		}
		x /= 10
	}
	return y
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	x := 1534236469
	fmt.Println(reverse(x))
}
