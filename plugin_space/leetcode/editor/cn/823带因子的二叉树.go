package main

import (
	"sort"
)

//leetcode submit region begin(Prohibit modification and deletion)
func numFactoredBinaryTrees(arr []int) int {
	sort.Ints(arr)
	var dict = make(map[int]int)
	for i, e := range arr {
		dict[e] = 0
		for j := 0; j <= i; j++ {
			f1 := arr[j]
			if e%f1 != 0 {
				continue
			}
			f2 := e / f1
			_, ok1 := dict[f1]
			_, ok2 := dict[f2]
			if ok1 && ok2 {
				dict[e] = dict[e] + dict[f1]*dict[f2]
			}
		}
		dict[e] = dict[e] + 1
	}

	var sum int = 0
	for _, v := range dict {
		sum = (sum + v) % (1e9 + 7)
	}
	return sum
}

//leetcode submit region end(Prohibit modification and deletion)
