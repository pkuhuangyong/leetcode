package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func consecutiveNumbersSum(n int) int {
	// 假设 n 可以被表示为以 a 为开头的连续 i 个正整数的和，则有：
	// n=a+a+1+……+(a+i-1)=a*i+i(i-1)/2
	// 从 1 开始枚举 a 和 i 的取值直到 (n+1)/2
	// 若满足 n==a*i+i(i-1)/2 则输出连续整数数列

	if n < 3 {
		return 1
	}
	res := make([][]int, 0)

	for a := 1; a <= (n+1)/2; a++ {
		for i := 1; i*i < n*2; i++ {
			if n == a*i+i*(i-1)/2 {
				res = append(res, buildSequence(a, i))
			}
		}
	}
	printSequences(n, res)
	fmt.Println("------------------------")
	return len(res) + 1
}
func buildSequence(a, i int) []int {
	res := make([]int, 0)
	for ; i > 0; i-- {
		res = append(res, a+i-1)
	}
	return res
}

func printSequences(n int, seq [][]int) {
	for _, sli := range seq {
		equation := fmt.Sprintf("%d=", n)
		for _, num := range sli {
			equation += fmt.Sprintf("%d", num) + "+"
		}
		equation = equation[:len(equation)-1]
		fmt.Println(equation)
	}
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {

	for n := 1; n < 20; n++ {
		fmt.Println(consecutiveNumbersSum(n))
	}
}
