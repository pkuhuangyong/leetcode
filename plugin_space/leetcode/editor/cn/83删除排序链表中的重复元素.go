package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func deleteDuplicates(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	pre, sub := head, head

	for sub != nil {
		if sub.Val != pre.Val {
			pre.Next = sub
			pre = sub
		}
		sub = sub.Next
	}
	pre.Next = sub
	return head
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	node5 := &ListNode{3, nil}
	node4 := &ListNode{3, node5}
	node3 := &ListNode{2, node4}
	node2 := &ListNode{1, node3}
	head := &ListNode{1, node2}
	deleteDuplicates(head)
}

type ListNode struct {
	Val  int
	Next *ListNode
}
