package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func leafSimilar(root1 *TreeNode, root2 *TreeNode) bool {
	leaves := make([]int, 0)

	var traverse func(root *TreeNode)
	traverse = func(root *TreeNode) {
		if root.Left == nil && root.Right == nil {
			leaves = append(leaves, root.Val)
		}
		if root.Left != nil {
			traverse(root.Left)
		}
		if root.Right != nil {
			traverse(root.Right)
		}
	}

	traverse(root1)
	leaves1 := make([]int, len(leaves))
	copy(leaves1, leaves)

	leaves = leaves[:0]
	traverse(root2)
	leaves2 := make([]int, len(leaves))
	copy(leaves2, leaves)
	return compare2Slices(leaves1, leaves2)
}

func compare2Slices(s1, s2 []int) bool {
	if len(s1) != len(s2) {
		return false
	}
	for i := 0; i < len(s1); i++ {
		if s1[i] != s2[i] {
			return false
		}
	}
	return true
}

//leetcode submit region end(Prohibit modification and deletion)
