package main

//leetcode submit region begin(Prohibit modification and deletion)
func minEatingSpeed(piles []int, h int) int {
	// 在 1 和 1e9 之间搜索一个速度最小值 k，满足 spendHour(k,piles)<=h
	lo, hi := 1, 1000000000+1

	for lo < hi {
		mid := lo + (hi-lo)/2

		if spendHour(mid, piles) <= h {
			hi = mid
		} else {
			lo = mid + 1
		}
	}
	return lo
}

func spendHour(velocity int, piles []int) int {
	hours := 0
	for i := 0; i < len(piles); i++ {
		h := piles[i] / velocity
		if piles[i]%velocity > 0 {
			h++
		}
		hours += h
	}
	return hours
}

//leetcode submit region end(Prohibit modification and deletion)
