package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
var valToIndex = make(map[int]int)

func constructFromPrePost(preorder []int, postorder []int) *TreeNode {
	for i, val := range postorder {
		valToIndex[val] = i
	}
	return build(preorder, 0, len(preorder)-1, postorder, 0, len(postorder)-1)
}
func build(preorder []int, preStart int, preEnd int,
	postorder []int, postStart int, postEnd int) *TreeNode {
	if preStart > preEnd {
		return nil
	}
	if preStart == preEnd {
		return &TreeNode{
			Val:   preorder[preStart],
			Left:  nil,
			Right: nil,
		}
	}
	rootVal := preorder[preStart]
	leftRootVal := preorder[preStart+1]
	leftRootValIdx := valToIndex[leftRootVal]
	leftSize := leftRootValIdx - postStart + 1
	root := &TreeNode{Val: rootVal}
	root.Left = build(preorder, preStart+1, preStart+leftSize, postorder, postStart, postStart+leftSize-1)
	root.Right = build(preorder, preStart+leftSize+1, preEnd, postorder, postStart+leftSize, postEnd-1)

	return root
}

//leetcode submit region end(Prohibit modification and deletion)
