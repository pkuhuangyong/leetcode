package main

import (
	"fmt"
	"math"
)

//leetcode submit region begin(Prohibit modification and deletion)
func myAtoi(s string) int {
	if len(s) == 0 {
		return 0
	}
	m := make(map[uint8]int32)
	m['0'] = 0
	m['1'] = 1
	m['2'] = 2
	m['3'] = 3
	m['4'] = 4
	m['5'] = 5
	m['6'] = 6
	m['7'] = 7
	m['8'] = 8
	m['9'] = 9

	var flag, res int32 = 1, 0
	i := 0
	for ; i < len(s); i++ {
		if s[i] != ' ' {
			break
		}
	}
	if i == len(s) {
		return 0
	}
	if s[i] == '.' {
		return 0
	}

	if s[i] == '-' {
		flag = -1
		for j := i + 1; j < len(s); j++ {
			if !('0' <= s[j] && s[j] <= '9') {
				return int(res * flag)
			}
			if math.MaxInt32/10 < res ||
				(math.MaxInt32/10 == res && math.MaxInt32%10 < m[s[j]]) {
				return math.MinInt32
			}
			res = res*10 + m[s[j]]
		}
	} else if s[i] == '+' {
		for j := i + 1; j < len(s); j++ {
			if !('0' <= s[j] && s[j] <= '9') {
				return int(res * flag)
			}
			if math.MaxInt32/10 < res ||
				(math.MaxInt32/10 == res && math.MaxInt32%10 < m[s[j]]) {
				return math.MaxInt32
			}
			res = res*10 + m[s[j]]
		}
	} else {
		for j := i; j < len(s); j++ {
			if !('0' <= s[j] && s[j] <= '9') {
				return int(res * flag)
			}

			if math.MaxInt32/10 < res ||
				(math.MaxInt32/10 == res && math.MaxInt32%10 < m[s[j]]) {
				return math.MaxInt32
			}
			res = res*10 + m[s[j]]
		}
	}

	return int(res * flag)

}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	s := "91283472332"
	fmt.Println(myAtoi(s))
}
