package main

import (
	"math"
)

//leetcode submit region begin(Prohibit modification and deletion)
type StockSpanner struct {
	stack [][]int
	days  int
}

func Constructor() StockSpanner {
	return StockSpanner{
		days:  -1,
		stack: [][]int{{-1, math.MaxInt64}},
	}
}

func (this *StockSpanner) Next(price int) int {
	for price >= this.stack[len(this.stack)-1][1] {
		this.stack = this.stack[:len(this.stack)-1]
	}
	lastDay := this.stack[len(this.stack)-1]

	this.days++
	span := this.days - lastDay[0]
	this.stack = append(this.stack, []int{this.days, price})
	return span
}

/**
 * Your StockSpanner object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Next(price);
 */
//leetcode submit region end(Prohibit modification and deletion)
