package main

import (
	"fmt"
	"sort"
)

//leetcode submit region begin(Prohibit modification and deletion)
func subsetsWithDup(nums []int) [][]int {
	sort.Ints(nums)
	ans := make([][]int, 0)
	ans = append(ans, []int{})

	var backtrack func(subSet []int, length int, start int, used []bool)
	backtrack = func(subSet []int, length int, start int, used []bool) {
		if len(subSet) == length {
			temp := make([]int, length)
			copy(temp, subSet)
			ans = append(ans, temp)
			return
		}

		for i := start; i < len(nums); i++ {
			if i > 0 && nums[i] == nums[i-1] && !used[i-1] {
				continue
			}
			subSet = append(subSet, nums[i])
			used[i] = true
			backtrack(subSet, length, i+1, used)
			subSet = subSet[:len(subSet)-1]
			used[i] = false
		}
	}

	for l := 1; l <= len(nums); l++ {
		used := make([]bool, len(nums))
		subSet := make([]int, 0)
		backtrack(subSet, l, 0, used)
	}

	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
func main() {
	nums := []int{1, 2, 2}
	fmt.Println(subsetsWithDup(nums))
}
