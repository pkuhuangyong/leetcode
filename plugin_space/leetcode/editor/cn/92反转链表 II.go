package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reverseBetween(head *ListNode, left int, right int) *ListNode {
	dummy := &ListNode{
		0,
		head,
	}
	p, l, r := dummy, dummy, dummy
	var n *ListNode
	for i := 0; i < left-1; i++ {
		p = p.Next
	}
	for i := 0; i < right; i++ {
		r = r.Next
	}
	l = p.Next
	p.Next = nil
	n = r.Next
	r.Next = nil
	reverseLinkedList(l)
	p.Next = r
	l.Next = n
	return dummy.Next
}

func reverseLinkedList(head *ListNode) {
	var pre *ListNode
	cur := head
	for cur != nil {
		next := cur.Next
		cur.Next = pre
		pre = cur
		cur = next
	}
}

//leetcode submit region end(Prohibit modification and deletion)
type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	node5 := &ListNode{5, nil}
	node4 := &ListNode{4, node5}
	node3 := &ListNode{3, node4}
	node2 := &ListNode{2, node3}
	node1 := &ListNode{1, node2}
	reverseBetween(node1, 2, 4)

}
