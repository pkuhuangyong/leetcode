package main

//leetcode submit region begin(Prohibit modification and deletion)
type RecentCounter struct {
	req3000 []int
}

func Constructor() RecentCounter {
	return RecentCounter{req3000: make([]int, 0)}
}

func (this *RecentCounter) Ping(t int) int {
	this.req3000 = append(this.req3000, t)
	if t <= 3001 {
		return len(this.req3000)
	}
	idx := binarySearch(this.req3000, t-3000, 0, len(this.req3000))
	this.req3000 = this.req3000[idx:]
	return len(this.req3000)
}

func binarySearch(reqs []int, target int, lo int, hi int) int {
	if lo > hi {
		return lo
	}

	mid := lo + (hi-lo)/2
	if reqs[mid] == target {
		return mid
	} else if reqs[mid] > target {
		return binarySearch(reqs, target, lo, mid-1)
	} else {
		return binarySearch(reqs, target, mid+1, hi)
	}
}

/**
 * Your RecentCounter object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Ping(t);
 */
//leetcode submit region end(Prohibit modification and deletion)
