package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

func inorderTraversal(root *TreeNode) []int {
	var ans = &[]int{}

	var inorder func(root *TreeNode, ans *[]int)
	inorder = func(root *TreeNode, ans *[]int) {
		if root == nil {
			return
		}
		inorder(root.Left, ans)
		*ans = append(*ans, root.Val)
		inorder(root.Right, ans)
	}
	inorder(root, ans)
	return *ans
}

//leetcode submit region end(Prohibit modification and deletion)
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	node3 := &TreeNode{Val: 3}
	node2 := &TreeNode{
		Val:   2,
		Left:  node3,
		Right: nil,
	}
	node1 := &TreeNode{
		Val:   1,
		Left:  nil,
		Right: node2,
	}
	res := inorderTraversal(node1)
	fmt.Println(res)
}
