package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func generateTrees(n int) []*TreeNode {
	return generateHelper(1, n)
}

func generateHelper(start, end int) []*TreeNode {
	if start > end {
		return []*TreeNode{nil}
	}
	var allTrees []*TreeNode
	for i := start; i <= end; i++ {
		leftTrees := generateHelper(start, i-1)
		rightTrees := generateHelper(i+1, end)
		for _, leftTree := range leftTrees {
			for _, rightTree := range rightTrees {
				curTree := &TreeNode{
					Val:   i,
					Left:  leftTree,
					Right: rightTree,
				}

				allTrees = append(allTrees, curTree)
			}
		}
	}
	return allTrees
}

//leetcode submit region end(Prohibit modification and deletion)
