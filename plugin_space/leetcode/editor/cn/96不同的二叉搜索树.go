package main

//leetcode submit region begin(Prohibit modification and deletion)
func numTrees(n int) int {
	dp := make([]int, n+1)
	dp[0] = 1
	dp[1] = 1
	for m := 2; m <= n; m++ {
		sum := 0
		for i := 1; i <= m; i++ {
			sum += dp[i-1] * dp[m-i]
		}
		dp[m] = sum
	}
	return dp[n]
}

//leetcode submit region end(Prohibit modification and deletion)

func main() {
	numTrees(3)
}
