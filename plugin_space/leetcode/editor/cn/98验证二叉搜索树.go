package main

import "math"

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isValidBST(root *TreeNode) bool {
	prev := math.MinInt64

	var inorder func(root *TreeNode) bool
	inorder = func(root *TreeNode) bool {
		if root == nil {
			return true
		}
		validLeft := inorder(root.Left)
		if root.Val <= prev {
			return false
		}
		prev = root.Val
		validRight := inorder(root.Right)
		return validRight && validLeft
	}
	//return valid(root, math.MinInt64, math.MaxInt64)
	return inorder(root)
}

func valid(root *TreeNode, lower int, upper int) bool {
	if root == nil {
		return true
	}

	if root.Val >= upper || root.Val <= lower {
		return false
	}

	return valid(root.Left, lower, root.Val) &&
		valid(root.Right, root.Val, upper)
}

//leetcode submit region end(Prohibit modification and deletion)
