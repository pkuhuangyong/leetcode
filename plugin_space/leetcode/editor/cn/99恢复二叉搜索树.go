package main

import "math"

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func recoverTree(root *TreeNode) {
	//nodes := make([]*TreeNode, 0)
	var inorder func(root *TreeNode)
	prev := &TreeNode{Val: math.MinInt64}
	var a, b *TreeNode

	inorder = func(root *TreeNode) {
		if root == nil {
			return
		}
		inorder(root.Left)
		if root.Val < prev.Val {
			b = root
			if a == nil {
				a = prev
			}
		}
		prev = root
		inorder(root.Right)
	}
	inorder(root)

	temp := a.Val
	a.Val = b.Val
	b.Val = temp
}

//leetcode submit region end(Prohibit modification and deletion)
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	node2 := &TreeNode{Val: 2}

	node1 := &TreeNode{
		Val:   1,
		Left:  nil,
		Right: nil,
	}
	node4 := &TreeNode{
		Val:   4,
		Left:  node2,
		Right: nil,
	}
	node3 := &TreeNode{
		Val:   3,
		Left:  node1,
		Right: node4,
	}
	recoverTree(node3)
}
