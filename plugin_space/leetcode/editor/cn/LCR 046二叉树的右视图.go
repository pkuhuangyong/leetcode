package main

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func rightSideView(root *TreeNode) []int {
	ans := make([]int, 0)

	if root == nil {
		return ans
	}
	var walk func(root *TreeNode, depth int)
	walk = func(root *TreeNode, depth int) {
		if root == nil {
			return
		}
		if len(ans) == depth {
			ans = append(ans, root.Val)
		}
		walk(root.Right, depth+1)
		walk(root.Left, depth+1)
	}
	walk(root, 0)
	return ans
}

//leetcode submit region end(Prohibit modification and deletion)
