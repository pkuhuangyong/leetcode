输入构成一棵树，本质上是求两个节点的最小公共父节点

输入中 region 的第一个元素表示父节点，之后的各个元素是其子节点

先将输入转换成 子节点:父节点 的表示形式，如实例输入转换成如下：

"Earth":"root"

"North America":"Earth"

"South America":"Earth"

"United States":"North America"

"Canada":"North America"

"New York":"United States"

"Boston":"United States"

"Ontario":"Canada"

"Quebec":"Canada"

"Brazil":"South America"

对于给定的两个区域 region1 region2 可以分别获得他们到根节点的路径

比较二者路径，输出二者第一个相同的节点即可


另一种思路使用双指针，本质上是求交叉链表的第一个公共节点

假设链表 a 的独有长度为 l1，链表 b 的独有长度为 l2，他们的公共长度为 c

那么指针 pa 沿着 a 链表走到末尾走过的长度为 l1+c，此时让 pa 指向链表 b 的头

指针 pb 沿着 b 链表走到末尾走过的长度为 l2+c，此时让 pb 指向链表 a 的头

两个指针同时移动，当二者都移动距离 l1+l2+c 时，他们就会在第一个公共节点相遇