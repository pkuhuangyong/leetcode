package main

//leetcode submit region begin(Prohibit modification and deletion)
func solveNQueens(n int) [][]string {
	chessBoards := make([][]string, 0)

	var backtrack func(chessBoard [][]string, n int, row int)
	backtrack = func(chessBoard [][]string, n int, row int) {
		// 行标超出 n，收集结果
		if row == n {
			solution := make([]string, 0)
			for _, r := range chessBoard {
				tempRow := ""
				for i := 0; i < n; i++ {
					tempRow += r[i]
				}
				solution = append(solution, tempRow)
			}
			chessBoards = append(chessBoards, solution)
			return
		}

		// 对每一个列进行枚举
		for i := 0; i < n; i++ {
			if !isValid(chessBoard, n, row, i) {
				continue
			}
			chessBoard[row][i] = "Q"

			// 对下一行的位置进行枚举
			backtrack(chessBoard, n, row+1)

			chessBoard[row][i] = "."

		}

	}
	chessBoard := buildChessBoard(n)
	backtrack(chessBoard, n, 0)
	return chessBoards
}

func buildChessBoard(n int) [][]string {
	chessBoard := make([][]string, n)
	for i := 0; i < n; i++ {
		row := make([]string, n)
		for j := 0; j < n; j++ {
			row[j] = "."
		}
		chessBoard[i] = row
	}
	return chessBoard
}

func isValid(chessBoard [][]string, n int, row int, col int) bool {

	// 验证行
	for _, point := range chessBoard[row] {
		if point == "Q" {
			return false
		}
	}

	// 验证列
	for i := 0; i < n; i++ {
		if chessBoard[i][col] == "Q" {
			return false
		}
	}

	// 验证 45° 角 右上方
	for i, j := row, col; i >= 0 && j < n; {
		if chessBoard[i][j] == "Q" {
			return false
		}
		i--
		j++
	}

	// 验证 45° 角 左下方
	for i, j := row, col; i < n && j >= 0; {
		if chessBoard[i][j] == "Q" {
			return false
		}
		i++
		j--
	}

	// 验证 45° 角 左上方
	for i, j := row, col; i >= 0 && j >= 0; {
		if chessBoard[i][j] == "Q" {
			return false
		}
		i--
		j--
	}

	// 验证 45° 角 右下方
	for i, j := row, col; i < n && j < n; {
		if chessBoard[i][j] == "Q" {
			return false
		}
		i++
		j++
	}

	return true
}

//leetcode submit region end(Prohibit modification and deletion)
