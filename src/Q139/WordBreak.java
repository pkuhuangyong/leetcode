package Q139;

import java.util.Arrays;
import java.util.List;

public class WordBreak {

    boolean[] dp;

    public WordBreak(int n) {
        this.dp = new boolean[n];
    }

    public boolean wordBreakDp(String s, List<String> wordDict) {
        dp[0] = true;
        for (int i = 1; i <= s.length(); i++) {
            for (int m = 0; m < i; m++) {
                String sub = s.substring(m, i);
                boolean temp = dp[m] && wordDict.contains(sub);
                if (temp) {
                    dp[i] = true;
                    break;
                }
            }
        }
        System.out.println(Arrays.toString(dp));
        return dp[s.length()];
    }

    public boolean wordBreakRecursion(String s, List<String> wordDict) {
        if (s.length() == 0) {
            return true;
        }
        for (String w : wordDict) {
            if (match(s, w)) {
                if (wordBreakRecursion(s.substring(w.length()), wordDict)) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean match(String s, String w) {
        if (s.length() < w.length()) {
            return false;
        } else {
            return s.startsWith(w);
        }
    }
}
