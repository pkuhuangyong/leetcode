package Q2218;

import java.util.List;

public class MaximumValueOfKCoinsFromPiles {

    public int maxValueOfCoins(List<List<Integer>> piles, int k) {
        int[][] sumTable = sumFirstK(piles);

        int[][] dp = new int[piles.size()][k + 1];

        for (int i = 0; i < piles.size(); i++) {
            if (i == 0) {
                for (int j = 1; j <= piles.get(i).size() && j <= k; j++) {
                    dp[i][j] = sumTable[i][j];
                }
            } else {
                for (int t = 1; t <= k; t++) {
                    dp[i][t] = dp[i - 1][t];
                    int maxTimesForStackI = Math.min(t, piles.get(i).size());
                    for (int j = maxTimesForStackI; j >= 0; j--) {
                        int cur = dp[i - 1][t - j] + sumTable[i][j];
                        if (dp[i][t] < cur) {
                            dp[i][t] = cur;
                        }
                    }
                }
            }
        }
        return dp[piles.size() - 1][k];
    }

    public int[][] sumFirstK(List<List<Integer>> piles) {
        int row = piles.size();
        int col = 0;
        for (List<Integer> pile : piles) {
            if (pile.size() > col) {
                col = pile.size();
            }
        }
        int[][] sumTable = new int[row][col + 1];
        for (int i = 0; i < row; i++) {
            for (int j = 1; j <= piles.get(i).size(); j++) {
                sumTable[i][j] = sumTable[i][j - 1] + piles.get(i).get(j - 1);
            }
        }

        return sumTable;
    }
}
