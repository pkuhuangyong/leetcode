package main

import "fmt"

//leetcode submit region begin(Prohibit modification and deletion)
func isPreorder(nodes [][]int) bool {
	var max = 0
	for _, node := range nodes {
		if max < node[0] {
			max = node[0]
		}
	}

	tree, root := buildTree(nodes, max)

	var preOrder = make([]int, 0)
	var f func(tree [][]int, root int)
	f = func(tree [][]int, root int) {
		preOrder = append(preOrder, root)
		if tree[root] == nil {
			return
		}

		for _, child := range tree[root] {
			f(tree, child)
		}
	}
	f(tree, root)

	for i, node := range preOrder {
		if node != nodes[i][0] {
			return false
		}
	}
	return true
}

func buildTree(nodes [][]int, max int) ([][]int, int) {

	res := make([][]int, max+1)
	root := 0

	for _, node := range nodes {
		if node[1] == -1 {
			root = node[0]
			res[node[0]] = []int{}
			continue
		}

		if res[node[1]] != nil {
			res[node[1]] = append(res[node[1]], node[0])
		} else {
			children := make([]int, 0)
			children = append(children, node[0])
			res[node[1]] = children
		}
	}

	return res, root
}

func preOrderTravel(tree [][]int, root int, preorder *[]int) {

	fmt.Println(root)
	*preorder = append(*preorder, root)

	if tree[root] == nil {
		return
	}

	for _, child := range tree[root] {
		preOrderTravel(tree, child, preorder)
	}
}

func main() {
	var nodes = [][]int{
		{8, -1},
		{1, 8},
		{2, 8},
		{3, 2},
		{4, 2},
		{5, 3},
		{6, 4},
	}

	isPreorder(nodes)
}

//leetcode submit region end(Prohibit modification and deletion)
