package main

import "strconv"

import (
	"strings"
)

// Definition for a binary tree node.
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}
type Visitor interface {
	Visit(node TreeNode, res *[]string) (nv Visitor)
}

var NullNode = TreeNode{
	Val:   -10086,
	Left:  nil,
	Right: nil,
}

func Walk(v Visitor, node TreeNode, res *[]string) {
	nv := v.Visit(node, res)
	if nv == nil {
		return
	}

	if node.Left == nil {
		Walk(nv, NullNode, res)
	} else {
		Walk(nv, *node.Left, res)
	}

	if node.Right == nil {
		Walk(nv, NullNode, res)
	} else {
		Walk(nv, *node.Right, res)
	}
}

type serializer int

func (sv serializer) Visit(node TreeNode, res *[]string) (nv Visitor) {

	if node.Val == -10086 {
		*res = append(*res, "null")
		return nil
	} else {
		*res = append(*res, strconv.Itoa(node.Val))
		return sv
	}
}

type Codec struct {
	S serializer
}

func Constructor() Codec {
	return Codec{S: 1}
}

// Serializes a tree to a single string.
func (this *Codec) serialize(root *TreeNode) string {
	if root == nil {
		return ""
	}
	var res []string
	Walk(this.S, *root, &res)
	return strings.Join(res, ",")
}

// Deserializes your encoded data to tree.
func (this *Codec) deserialize(data string) *TreeNode {
	if data == "" {
		return nil
	}
	split := strings.Split(data, ",")
	return Deserialize(&split)
}

func Deserialize(s *[]string) *TreeNode {
	if (*s)[0] == "null" {
		*s = (*s)[1:]
		return nil
	}

	val, _ := strconv.Atoi((*s)[0])
	root := TreeNode{
		Val:   val,
		Left:  nil,
		Right: nil,
	}
	*s = (*s)[1:]
	root.Left = Deserialize(s)
	root.Right = Deserialize(s)

	return &root
}
