package main

import (
	"testing"
)

var node4,
	node3,
	node2,
	node1 = TreeNode{
	Val:   4,
	Left:  nil,
	Right: nil,
}, TreeNode{
	Val:   3,
	Left:  nil,
	Right: nil,
}, TreeNode{
	Val:   2,
	Left:  nil,
	Right: &node4,
}, TreeNode{
	Val:   1,
	Left:  &node2,
	Right: &node3,
}

var obj = Constructor()

func TestSerialize(t *testing.T) {
	cases := []struct {
		Name     string
		Tree     *TreeNode
		Expected string
	}{
		{"empty tree", nil, ""},
		{"normal tree", &node1, "1,2,null,4,null,null,3,null,null"},
	}

	for _, c := range cases {
		if ans := obj.serialize(c.Tree); ans != c.Expected {
			t.Fatalf("\nserialize %s:\n\t%v\nexpected:\t%s\nbut got:\t%s",
				c.Name, c.Tree, c.Expected, ans)
		}
	}
}

func TestDeserialize(t *testing.T) {
	cases := []struct {
		Name     string
		Str      string
		Expected string
	}{
		{"empty string", "", ""},
		{"normal string", "1,2,null,4,null,null,3,null,null", "1,2,null,4,null,null,3,null,null"},
	}

	for _, c := range cases {
		root := obj.deserialize(c.Str)
		serializeRes := obj.serialize(root)

		if c.Expected != serializeRes {
			t.Fatalf("\ndeserialize %s:\n\t%v\nexpected:\t%s\nbut got:\t%s",
				c.Name, c.Str, c.Expected, serializeRes)
		}
	}
}
