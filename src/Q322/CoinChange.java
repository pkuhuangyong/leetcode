package Q322;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CoinChange {

    public int coinChange(int[] coins, int amount) {
        Integer[] coinArray = Arrays.stream(coins).boxed().sorted().toArray(Integer[]::new);
        Set<Integer> coinSet = new HashSet<>(Arrays.asList(coinArray));

        int[] dp = new int[amount + 1];
        dp[0] = 0;

        for (int i = 1; i <= amount; i++) {
            dp[i] = Integer.MAX_VALUE;
            if (coinSet.contains(i)) {
                dp[i] = 1;
            } else if (i < coinArray[0]) {
                dp[i] = 100000;
            } else {
                for (int j = 0; j < coinArray.length && coinArray[j] <= i; j++) {
                    if (dp[i - coinArray[j]] + 1 < dp[i]) {
                        dp[i] = dp[i - coinArray[j]] + 1;
                    }
                }
            }
        }

        return dp[amount] >= 100000 ? -1 : dp[amount];
    }
}
