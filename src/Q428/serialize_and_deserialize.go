package main

import (
	"fmt"
	"strconv"
	"strings"
)

type Node struct {
	Val      int
	Children []*Node
}

var NullNode Node = Node{
	Val:      -1,
	Children: nil,
}

type Visitor interface {
	Visit(node Node, res *[]string) (nv Visitor)
}

func Walk(v Visitor, node Node, res *[]string) {
	nv := v.Visit(node, res)
	if nv == nil {
		return
	}

	for _, cn := range node.Children {
		Walk(nv, *cn, res)
	}

	nv.Visit(NullNode, res)
}

type serializer int

func (sv serializer) Visit(node Node, res *[]string) (nv Visitor) {

	var va string
	if node.Val == -1 {
		va = "null"
	} else {
		va = strconv.Itoa(node.Val)
	}
	*res = append(*res, va)
	return sv
}

type Codec struct {
	s serializer
}

func Constructor() *Codec {
	return &Codec{s: 0}
}

func (this *Codec) serialize(root *Node) string {
	if root == nil {
		return ""
	}
	res := make([]string, 0)
	Walk(this.s, *root, &res)
	return strings.Join(res, ",")
}

func (this *Codec) deserialize(data string) *Node {
	if len(data) == 0 {
		return nil
	}

	var stk Stack = &stack{container: make([]*Node, 0, 16)}

	var root *Node

	values := strings.Split(data, ",")
	for _, va := range values {
		if va == "null" {
			if (stk).Size() == 1 {
				root = stk.Pop()
				return root
			}

			child := stk.Pop()
			parent := stk.Pop()
			parent.Children = append(parent.Children, child)
			stk.Push(parent)
		} else {
			vi, _ := strconv.Atoi(va)
			node := Node{
				Val:      vi,
				Children: nil,
			}
			stk.Push(&node)
		}
	}
	return root
}

func main() {
	node6 := Node{
		Val:      6,
		Children: nil,
	}
	node5 := Node{
		Val:      5,
		Children: nil,
	}
	node2 := Node{
		Val:      2,
		Children: nil,
	}
	node4 := Node{
		Val:      4,
		Children: nil,
	}
	node3 := Node{
		Val:      3,
		Children: []*Node{&node5, &node6},
	}
	node1 := Node{
		Val:      1,
		Children: []*Node{&node3, &node2, &node4},
	}
	node1.Val = 1

	obj := Constructor()
	res := obj.serialize(&node1)
	fmt.Println("-#- SERIALIZATION -#-")
	fmt.Println(res)

	fmt.Println("-*- DESERIALIZATION -*-")
	root := obj.deserialize(res)
	fmt.Printf("%v\n", root)

	res2 := obj.serialize(root)
	fmt.Println("-#- RESERIALIZATION -#-")
	fmt.Println(res2)
}
