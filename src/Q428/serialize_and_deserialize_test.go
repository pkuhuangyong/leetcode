package main

import (
	"testing"
)

var node6 = Node{
	Val:      6,
	Children: nil,
}
var node5 = Node{
	Val:      5,
	Children: nil,
}
var node2 = Node{
	Val:      2,
	Children: nil,
}
var node4 = Node{
	Val:      4,
	Children: nil,
}
var node3 = Node{
	Val:      3,
	Children: []*Node{&node5, &node6},
}
var node1 = Node{
	Val:      1,
	Children: []*Node{&node3, &node2, &node4},
}

var obj = Constructor()

func TestSerialize(t *testing.T) {
	cases := []struct {
		Name     string
		Tree     *Node
		Expected string
	}{
		{"empty tree", nil, ""},
		{"3-ary tree", &node1, "1,3,5,null,6,null,null,2,null,4,null,null"},
	}

	for _, c := range cases {

		if res := obj.serialize(c.Tree); res != c.Expected {
			t.Fatalf("\nserialize %s:\n\t%v\nexpected: \t%s\nbut got: \t%s", c.Name, *(c.Tree), c.Expected, res)
		}
	}
}

func TestDeserialize(t *testing.T) {
	cases := []struct {
		Name     string
		Str      string
		Expected string
	}{
		{"empty string", "", ""},
		{
			"normal string",
			"1,3,5,null,6,null,null,2,null,4,null,null",
			"1,3,5,null,6,null,null,2,null,4,null,null"},
	}

	for _, c := range cases {
		res := obj.deserialize(c.Str)
		serializeRes := obj.serialize(res)
		if c.Expected != serializeRes {
			t.Fatalf("\ndeserialize %s:\n\t%s\nexpected:\t %s\nbut got:\t %s", c.Name, c.Str, c.Expected, serializeRes)
		}
	}
}
