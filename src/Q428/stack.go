package main

type Stack interface {
	Push(element *Node)
	Pop() *Node
	Size() int
}

type stack struct {
	container []*Node
}

func (s *stack) Push(element *Node) {
	(*s).container = append((*s).container, element)
}

func (s *stack) Pop() *Node {
	c := (*s).container
	element := c[len(c)-1]
	(*s).container = c[:len(c)-1]
	return element
}

func (s *stack) Size() int {
	return len((*s).container)
}
