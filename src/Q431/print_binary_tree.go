package main

import (
	"strconv"
	"strings"
)

type Visitor interface {
	Visit(node TreeNode, res *[]string) (nv Visitor)
}

var NullNode = TreeNode{
	Val:   -10086,
	Left:  nil,
	Right: nil,
}

func Walk(v Visitor, node TreeNode, res *[]string) {
	nv := v.Visit(node, res)
	if nv == nil {
		return
	}

	if node.Left == nil {
		Walk(nv, NullNode, res)
	} else {
		Walk(nv, *node.Left, res)
	}

	if node.Right == nil {
		Walk(nv, NullNode, res)
	} else {
		Walk(nv, *node.Right, res)
	}
}

type serializer int

func (sv serializer) Visit(node TreeNode, res *[]string) (nv Visitor) {

	if node.Val == -10086 {
		*res = append(*res, "null")
		return nil
	} else {
		*res = append(*res, strconv.Itoa(node.Val))
		return sv
	}
}

type Codec struct {
	S serializer
}

func Constructor() Codec {
	return Codec{S: 1}
}

// Serializes a tree to a single string.
func (this *Codec) serialize(root *TreeNode) string {
	if root == nil {
		return ""
	}
	var res []string
	Walk(this.S, *root, &res)
	return strings.Join(res, ",")
}

var node6 = Node{
	Val:      6,
	Children: nil,
}
var node5 = Node{
	Val:      5,
	Children: nil,
}
var node2 = Node{
	Val:      2,
	Children: nil,
}
var node4 = Node{
	Val:      4,
	Children: nil,
}
var node3 = Node{
	Val:      3,
	Children: []*Node{&node5, &node6},
}
var node1 = Node{
	Val:      1,
	Children: []*Node{&node3, &node2, &node4},
}
