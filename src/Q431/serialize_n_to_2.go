package main

import "fmt"

type Node struct {
	Val      int
	Children []*Node
}

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func encode(root *Node) *TreeNode {
	if root == nil {
		return nil
	}

	biRoot := &TreeNode{
		Val:   root.Val,
		Left:  nil,
		Right: nil,
	} // 以当前节点构造二叉树根节点

	if len(root.Children) == 0 {
		return biRoot
	} else {
		biRoot.Left = encode(root.Children[0]) // 二叉树根节点的左孩子是编码后的 N 叉树的的第一棵子树
		// 根节点没有右孩子
		cur := biRoot.Left
		for i := 1; i < len(root.Children); i++ {
			cur.Right = encode(root.Children[i])
			cur = cur.Right
		}
	}
	return biRoot
}

func decode(root *TreeNode) *Node {
	if root == nil {
		return nil
	}

	nRoot := &Node{
		Val:      root.Val,
		Children: nil,
	}

	cur := root.Left

	for cur != nil {
		nRoot.Children = append(nRoot.Children, decode(cur))
		cur = cur.Right
	}

	return nRoot
}

func main() {
	obj := Constructor()
	biRoot := encode(&node1)
	fmt.Println("encode and serialize: ", obj.serialize(biRoot))

	nRoot := decode(biRoot)

	fmt.Printf("%v", nRoot)
}
